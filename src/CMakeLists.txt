add_library(whatwg_infra
    base64.cpp
)
target_compile_options(whatwg_infra PRIVATE
    -Wall -Wextra -pedantic -Wconversion -Wsign-conversion
)
target_include_directories(whatwg_infra PUBLIC
    ${PROJECT_SOURCE_DIR}/include
)

if (BUILD_TEST AND TEST_COVERAGE)
    target_compile_options(whatwg_infra PUBLIC
        -O0 -g --coverage
    )
    target_link_libraries(whatwg_infra
        --coverage
    )
endif()
