#include <whatwg/infra/base64.hpp>

#include <whatwg/infra/byte_sequence.hpp>

#include <algorithm>
#include <array>
#include <cassert>

namespace whatwg::infra {

static char const table[] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    "abcdefghijklmnopqrstuvwxyz"
    "0123456789+/";

auto constexpr table_len = sizeof(table) / sizeof(table[0]);

static code_point_t
lookup_code_point(size_t const i)
{
    assert(i < table_len);
    return static_cast<code_point_t>(table[i]);
}

static optional<size_t>
lookup_index(code_point_t const c)
{
    auto const b = to_byte(c);

    if ('0' <= b && b <= '9') {
        return 52 + (b - '0');
    }
    else if ('A' <= b && b <= 'Z') {
        return 0 + (b - 'A');
    }
    else if ('a' <= b && b <= 'z') {
        return 26 + (b - 'a');
    }
    else if (b == '+') {
        return 62;
    }
    else {
        assert(b == '/');
        return 63;
    }
}

static void
encode(unsigned char* const input, size_t const len, string_t& result)
{
    assert(len <= 3);
    unsigned char output[4];

    if (len == 0) return;

    for (size_t i = len; i < 3; ++i) {
        input[i] = 0x00;
    }

    output[0] = (input[0] & 0xFC) >> 2;
    output[1] = static_cast<unsigned char>(
        ((input[0] & 0x03u) << 4u) + ((input[1] & 0xF0u) >> 4u));
    output[2] = static_cast<unsigned char>(
        ((input[1] & 0x0F) << 2) + ((input[2] & 0xC0) >> 6));
    output[3] = input[2] & 0x3F;

    switch (len) {
        case 1:
            append(result, lookup_code_point(output[0]));
            append(result, lookup_code_point(output[1]));
            append(result, U"==");
            break;
        case 2:
            append(result, lookup_code_point(output[0]));
            append(result, lookup_code_point(output[1]));
            append(result, lookup_code_point(output[2]));
            append(result, U'=');
            break;
        case 3:
            append(result, lookup_code_point(output[0]));
            append(result, lookup_code_point(output[1]));
            append(result, lookup_code_point(output[2]));
            append(result, lookup_code_point(output[3]));
            break;
    }
}

template<size_t N>
class bit_buffer
{
public:
    bit_buffer()
    {
        clear();
    }

    void append(size_t const n)
    {
        assert(n < UINT8_MAX);

        auto const b = static_cast<uint8_t>(n) & 0x3F; // 6 bits
        // i = byte index of the buffer
        // j = number of bits already used in m_buffer[i]
        // k = number of shifts it has to apply to the byte
        //     i  j   k  k+j
        //  0: 0, 0,  2    2
        //  6: 0, 6, -4    2
        // 12: 1, 4, -2    2
        // 18: 2, 2, 0
        // 24: 2, 8
        auto const i = static_cast<int>(m_length) / 8;
        auto const j = static_cast<int>(m_length) % 8;
        auto const k = 2 - j;

        if (k >= 0) {
            m_buffer[i] = static_cast<uint8_t>(
                m_buffer[i] | ((b << k) & 0xFF));
        }
        else {
            m_buffer[i] = static_cast<uint8_t>(
                m_buffer[i] | ((b >> -k) & 0xFF));

            auto const mask = (1 << -k) - 1;
            auto const b2 = b & mask;
            m_buffer[i + 1] = static_cast<uint8_t>((b2 << (8 + k)) & 0xFF);
        }

        m_length += 6;
        assert(m_length <= N);
    }

    void clear()
    {
        for (size_t i = 0; i < N/8; ++i) {
            m_buffer[i] = 0;
        }
        m_length = 0;
    }

    size_t size() const { return m_length; }
    bool empty() const { return m_length == 0; }

    bit_buffer& operator>>=(size_t const n)
    {
        assert(n < 8);
        m_length -= n;

        return *this;
    }

    byte_sequence_view_t to_byte_sequence_view() const
    {
        return {
            reinterpret_cast<char const*>(m_buffer),
            m_length / 8
        };
    }

private:
    uint8_t m_buffer[N / 8];
    size_t m_length = 0;
};

string_t
forgiving_base64_encode(byte_sequence_view_t const data)
{
    std::array<unsigned char, 3> input;
    string_t result;
    size_t i = 0;

    for (auto const c: data) {
        input[i++] = static_cast<unsigned char>(c);

        if (i == input.size()) {
            encode(input.data(), input.size(), result);
            i = 0;
        }
    }

    encode(input.data(), i, result);

    return result;
}

optional<byte_sequence_t>
forgiving_base64_decode(string_view_t const data_)
{
    string_t data { data_ };
    remove(data, static_cast<bool(*)(code_point_t)>(is_ascii_whitespace));

    if (length(data) % 4 == 0) {
        if (ends_with(data, U"==")) {
            remove(data, length(data) - 2, 2);
        }
        else if (ends_with(data, U"=")) {
            remove(data, length(data) - 1, 1);
        }
    }

    if (length(data) % 4 == 1) return {};

    auto is_invalid = [](auto const c) {
        return !is_ascii_alphanumeric(c) && c != U'\x2B' && c != U'\x2F';
    };

    if (contains(data, is_invalid)) return {};

    byte_sequence_t output;
    bit_buffer<24> buffer;
    auto const past_the_end = length(data);

    position_t position = 0;
    while (position < past_the_end) {
        auto const code_point = data[position];
        auto const n = lookup_index(code_point);
        assert(n);

        buffer.append(*n);
        if (buffer.size() == 24) {
            append(output, buffer.to_byte_sequence_view());
            buffer.clear();
        }

        ++position;
    }

    if (!buffer.empty()) {
        if (auto const size = buffer.size(); size == 12) {
            buffer >>= 4;
            append(output, buffer.to_byte_sequence_view());
        }
        else {
            assert(size == 18);
            buffer >>= 2;
            append(output, buffer.to_byte_sequence_view());
        }
    }

    return output;
}

} // namespace whatwg::infra
