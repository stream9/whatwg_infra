#include <whatwg/infra/byte_sequence.hpp>

#include <iterator>

#include <boost/test/unit_test.hpp>

namespace whatwg::infra::testing {

BOOST_AUTO_TEST_SUITE(byte_sequence_)

    BOOST_AUTO_TEST_CASE(length_)
    {
        byte_sequence_t s;
        BOOST_CHECK_EQUAL(length(s), 0);

        s = "foo";
        BOOST_CHECK_EQUAL(length(s), 3);
    }

    BOOST_AUTO_TEST_CASE(is_empty_)
    {
        byte_sequence_t s;
        BOOST_CHECK(is_empty(s));

        s = "ABC";
        BOOST_CHECK(!is_empty(s));
    }

    BOOST_AUTO_TEST_CASE(byte_lowercase_)
    {
        byte_sequence_t s = "\x01\x1F ABCabc123\x80\xFF";

        byte_lowercase(s);
        BOOST_CHECK_EQUAL(s, "\x01\x1F abcabc123\x80\xFF");
    }

    BOOST_AUTO_TEST_CASE(byte_lowercase_copy_)
    {
        byte_sequence_t s1 = "\x01\x1F ABCabc123\x80\xFF";

        auto const s2 = byte_lowercase_copy(s1);
        BOOST_CHECK_EQUAL(s2, "\x01\x1F abcabc123\x80\xFF");
    }

    BOOST_AUTO_TEST_CASE(byte_uppercase_)
    {
        byte_sequence_t s = "\x01\x1F ABCabc123\x80\xFF";

        byte_uppercase(s);
        BOOST_CHECK_EQUAL(s, "\x01\x1F ABCABC123\x80\xFF");
    }

    BOOST_AUTO_TEST_CASE(byte_uppercase_copy_)
    {
        byte_sequence_t s1 = "\x01\x1F ABCabc123\x80\xFF";

        auto const s2 = byte_uppercase_copy(s1);
        BOOST_CHECK_EQUAL(s2, "\x01\x1F ABCABC123\x80\xFF");
    }

    BOOST_AUTO_TEST_CASE(byte_case_insensitive_match_1)
    {
        byte_sequence_t const s1;
        byte_sequence_t const s2;

        BOOST_CHECK(byte_case_insensitive_match(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(byte_case_insensitive_match_2)
    {
        byte_sequence_t const s1 = "abcDEF";
        byte_sequence_t const s2 = "ABCdef";

        BOOST_CHECK(byte_case_insensitive_match(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(byte_case_insensitive_match_3)
    {
        byte_sequence_t const s1 = "abcDEF";
        byte_sequence_t const s2 = "ABCdefg";

        BOOST_CHECK(!byte_case_insensitive_match(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(isomorphic_decode_1)
    {
        byte_sequence_t const input;

        auto const output = isomorphic_decode(input);

        BOOST_CHECK(output == U"");
    }

    BOOST_AUTO_TEST_CASE(isomorphic_decode_2)
    {
        byte_sequence_t const input = "byte";

        auto const output = isomorphic_decode(input);

        BOOST_CHECK(output == U"byte");
    }

    BOOST_AUTO_TEST_CASE(append_byte)
    {
        byte_sequence_t s;

        append(s, 'a');
        BOOST_CHECK_EQUAL(s, "a");

        append(s, '\0');
        BOOST_CHECK_EQUAL(s, "a\0"_sv);
    }

    BOOST_AUTO_TEST_CASE(append_view)
    {
        byte_sequence_t s;

        append(s, "ABC"_sv);
        BOOST_CHECK_EQUAL(s, "ABC"_sv);

        append(s, ""_sv);
        BOOST_CHECK_EQUAL(s, "ABC"_sv);

        append(s, "\0"_sv);
        BOOST_CHECK_EQUAL(s, "ABC\0"_sv);
    }

    BOOST_AUTO_TEST_CASE(prepend_byte)
    {
        byte_sequence_t s;

        prepend(s, 'a');
        BOOST_CHECK_EQUAL(s, "a");

        prepend(s, '\0');
        BOOST_CHECK_EQUAL(s, "\0a"_sv);
    }

    BOOST_AUTO_TEST_CASE(prepend_view)
    {
        byte_sequence_t s;

        prepend(s, "ABC"_sv);
        BOOST_CHECK_EQUAL(s, "ABC"_sv);

        prepend(s, ""_sv);
        BOOST_CHECK_EQUAL(s, "ABC"_sv);

        prepend(s, "\0"_sv);
        BOOST_CHECK_EQUAL(s, "\0ABC"_sv);
    }

    BOOST_AUTO_TEST_CASE(repalce_)
    {
        byte_sequence_t s;

        replace(s, [](auto c) { return c == 'A'; }, 'B');
        BOOST_CHECK(is_empty(s));

        s = "ABC";
        replace(s, [](auto c) { return c == 'A'; }, 'B');
        BOOST_CHECK_EQUAL(s, "BBC");

        replace(s, [](auto c) { return c == 'B'; }, 'C');
        BOOST_CHECK_EQUAL(s, "CCC");
    }

    BOOST_AUTO_TEST_CASE(insert_byte)
    {
        byte_sequence_t s;

        insert(s, 1, 'A');
        BOOST_CHECK(is_empty(s));

        insert(s, 0, '1');
        BOOST_CHECK_EQUAL(s, "1");

        insert(s, 0, '2');
        BOOST_CHECK_EQUAL(s, "21");

        insert(s, 2, '3');
        BOOST_CHECK_EQUAL(s, "213");

        insert(s, 1, '4');
        BOOST_CHECK_EQUAL(s, "2413");
    }

    BOOST_AUTO_TEST_CASE(insert_view)
    {
        byte_sequence_t s;

        insert(s, 1, "A");
        BOOST_CHECK(is_empty(s));

        insert(s, 0, "11");
        BOOST_CHECK_EQUAL(s, "11");

        insert(s, 0, "22");
        BOOST_CHECK_EQUAL(s, "2211");

        insert(s, 4, "33");
        BOOST_CHECK_EQUAL(s, "221133");

        insert(s, 1, "44");
        BOOST_CHECK_EQUAL(s, "24421133");
    }

    BOOST_AUTO_TEST_CASE(remove_1)
    {
        byte_sequence_t s;

        remove(s, 0, 2);
        BOOST_CHECK(is_empty(s));

        s = "ABC";
        remove(s, 1, 1);
        BOOST_CHECK_EQUAL(s, "AC");

        remove(s, 0, 3);
        BOOST_CHECK(is_empty(s));
    }

    BOOST_AUTO_TEST_CASE(remove_2)
    {
        byte_sequence_t s;

        remove(s, [](auto c) { return c == 'A'; });
        BOOST_CHECK(is_empty(s));

        s = "ABC";
        remove(s, [](auto c) { return c == 'A'; });
        BOOST_CHECK_EQUAL(s, "BC");

        remove(s, [](auto c) { return is_ascii_alpha(c); });
        BOOST_CHECK(is_empty(s));
    }

    BOOST_AUTO_TEST_CASE(assign_)
    {
        byte_sequence_t s;

        assign(s, "ABC");
        BOOST_CHECK_EQUAL(s, "ABC");

        assign(s, "CBA");
        BOOST_CHECK_EQUAL(s, "CBA");

        assign(s, "");
        BOOST_CHECK_EQUAL(s, "");
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        byte_sequence_t s;

        empty(s);
        BOOST_CHECK(is_empty(s));

        s = "55555";
        empty(s);
        BOOST_CHECK(is_empty(s));
    }

    BOOST_AUTO_TEST_CASE(contains_byte)
    {
        byte_sequence_t s;
        BOOST_CHECK(!contains(s, 'A'));

        s = "ABC";
        BOOST_CHECK( contains(s, 'A'));
        BOOST_CHECK(!contains(s, 'a'));
    }

    BOOST_AUTO_TEST_CASE(contains_cond)
    {
        byte_sequence_t s;
        BOOST_CHECK(!contains(s, [](auto c) { return c == 'A'; }));

        s = "ABC";
        BOOST_CHECK( contains(s, [](auto c) { return c == 'A'; }));
        BOOST_CHECK(!contains(s, [](auto c) { return c == 'a'; }));
    }

    BOOST_AUTO_TEST_CASE(starts_with_view)
    {
        BOOST_CHECK( starts_with("ABC"_sv, "AB"_sv));
        BOOST_CHECK( starts_with("ABC"_sv, "ABC"_sv));
        BOOST_CHECK(!starts_with("ABC"_sv, "ABCD"_sv));
        BOOST_CHECK(!starts_with("ABC"_sv, "BC"_sv));
    }

    BOOST_AUTO_TEST_CASE(starts_with_byte)
    {
        BOOST_CHECK(!starts_with(""_sv,    'A'));
        BOOST_CHECK( starts_with("ABC"_sv, 'A'));
        BOOST_CHECK(!starts_with("ABC"_sv, 'B'));
    }

    BOOST_AUTO_TEST_CASE(istarts_with_view)
    {
        BOOST_CHECK( istarts_with("ABC"_sv, "AB"_sv));
        BOOST_CHECK( istarts_with("ABC"_sv, "ab"_sv));
        BOOST_CHECK( istarts_with("ABC"_sv, "ABC"_sv));
        BOOST_CHECK( istarts_with("ABC"_sv, "abc"_sv));
        BOOST_CHECK(!istarts_with("ABC"_sv, "ABCD"_sv));
        BOOST_CHECK(!istarts_with("ABC"_sv, "abcd"_sv));
        BOOST_CHECK(!istarts_with("ABC"_sv, "BC"_sv));
        BOOST_CHECK(!istarts_with("ABC"_sv, "bc"_sv));
    }

    BOOST_AUTO_TEST_CASE(istarts_with_byte)
    {
        BOOST_CHECK(!istarts_with(""_sv,    'A'));
        BOOST_CHECK(!istarts_with(""_sv,    'a'));
        BOOST_CHECK( istarts_with("ABC"_sv, 'A'));
        BOOST_CHECK( istarts_with("ABC"_sv, 'a'));
        BOOST_CHECK(!istarts_with("ABC"_sv, 'B'));
        BOOST_CHECK(!istarts_with("ABC"_sv, 'b'));
    }

    BOOST_AUTO_TEST_CASE(ends_with_view)
    {
        BOOST_CHECK( ends_with("ABC"_sv, "BC"_sv));
        BOOST_CHECK( ends_with("ABC"_sv, "ABC"_sv));
        BOOST_CHECK(!ends_with("ABC"_sv, "DABC"_sv));
        BOOST_CHECK(!ends_with("ABC"_sv, "AB"_sv));
    }

    BOOST_AUTO_TEST_CASE(ends_with_byte)
    {
        BOOST_CHECK(!ends_with(""_sv,    'A'));
        BOOST_CHECK( ends_with("ABC"_sv, 'C'));
        BOOST_CHECK(!ends_with("ABC"_sv, 'A'));
    }

    BOOST_AUTO_TEST_CASE(iends_with_view)
    {
        BOOST_CHECK( iends_with("ABC"_sv, "BC"_sv));
        BOOST_CHECK( iends_with("ABC"_sv, "bc"_sv));
        BOOST_CHECK( iends_with("ABC"_sv, "ABC"_sv));
        BOOST_CHECK( iends_with("ABC"_sv, "abc"_sv));
        BOOST_CHECK(!iends_with("ABC"_sv, "DABC"_sv));
        BOOST_CHECK(!iends_with("ABC"_sv, "dabc"_sv));
        BOOST_CHECK(!iends_with("ABC"_sv, "AB"_sv));
        BOOST_CHECK(!iends_with("ABC"_sv, "ab"_sv));
    }

    BOOST_AUTO_TEST_CASE(iends_with_byte)
    {
        BOOST_CHECK(!iends_with(""_sv,    'A'));
        BOOST_CHECK(!iends_with(""_sv,    'a'));
        BOOST_CHECK( iends_with("ABC"_sv, 'C'));
        BOOST_CHECK( iends_with("ABC"_sv, 'c'));
        BOOST_CHECK(!iends_with("ABC"_sv, 'A'));
        BOOST_CHECK(!iends_with("ABC"_sv, 'a'));
    }

    BOOST_AUTO_TEST_CASE(default_constructor)
    {
        byte_sequence_t s;
        BOOST_CHECK(is_empty(s));
    }

    template<typename T>
    static void
    test_copy_constructor()
    {
        T const from = "ABC";
        byte_sequence_t s { from };

        BOOST_CHECK_EQUAL(s, from);
    }

    BOOST_AUTO_TEST_CASE(copy_constructor)
    {
        test_copy_constructor<byte_sequence_t>();
        test_copy_constructor<byte_sequence_view_t>();
        test_copy_constructor<std::string>();
        test_copy_constructor<std::string_view>();
        test_copy_constructor<char const*>();
    }

    template<typename T>
    static void
    test_move_constructor()
    {
        T from = "ABC";
        byte_sequence_t s { std::move(from) };

        BOOST_CHECK_EQUAL(s, "ABC");
    }

    BOOST_AUTO_TEST_CASE(move_constructor)
    {
        test_move_constructor<byte_sequence_t>();
        test_move_constructor<std::string>();
    }

    template<typename T>
    static void
    test_copy_assignment()
    {
        T const from = "ABC";
        byte_sequence_t s;
        s = from;

        BOOST_CHECK_EQUAL(s, from);
    }

    BOOST_AUTO_TEST_CASE(copy_assignment)
    {
        test_copy_assignment<byte_sequence_t>();
        test_copy_assignment<byte_sequence_view_t>();
        test_copy_assignment<std::string>();
        test_copy_assignment<std::string_view>();
        test_copy_assignment<char const*>();
    }

    template<typename T>
    static void
    test_move_assignment()
    {
        T from = "ABC";
        byte_sequence_t s;
        s = std::move(from);

        BOOST_CHECK_EQUAL(s, "ABC");
    }

    BOOST_AUTO_TEST_CASE(move_assignment)
    {
        test_move_assignment<byte_sequence_t>();
        test_move_assignment<std::string>();
    }

BOOST_AUTO_TEST_SUITE_END() // byte_sequence_

BOOST_AUTO_TEST_SUITE(byte_sequence_view_)

    BOOST_AUTO_TEST_CASE(default_constructor)
    {
        byte_sequence_view_t s;
        BOOST_CHECK(is_empty(s));
    }

    template<typename T>
    static void
    test_copy_constructor()
    {
        T const from = "ABC";
        byte_sequence_view_t s { from };

        BOOST_CHECK_EQUAL(s, from);
    }

    BOOST_AUTO_TEST_CASE(copy_constructor)
    {
        test_copy_constructor<byte_sequence_t>();
        test_copy_constructor<byte_sequence_view_t>();
        test_copy_constructor<std::string>();
        test_copy_constructor<std::string_view>();
        test_copy_constructor<char const*>();
    }

    template<typename T>
    static void
    test_copy_assignment()
    {
        T const from = "ABC";
        byte_sequence_view_t s;
        s = from;

        BOOST_CHECK_EQUAL(s, from);
    }

    BOOST_AUTO_TEST_CASE(copy_assignment)
    {
        test_copy_assignment<byte_sequence_t>();
        test_copy_assignment<byte_sequence_view_t>();
        test_copy_assignment<std::string>();
        test_copy_assignment<std::string_view>();
        test_copy_assignment<char const*>();
    }

    template<typename T>
    static void
    test_construct_from_iterator()
    {
        T const from = "ABC";
        byte_sequence_view_t s { std::begin(from), std::end(from) };

        BOOST_CHECK_EQUAL(s, from);
    }

    BOOST_AUTO_TEST_CASE(construct_from_iterator)
    {
        test_construct_from_iterator<byte_sequence_t>();
        test_construct_from_iterator<byte_sequence_view_t>();
        test_copy_assignment<std::string>();
        test_copy_assignment<std::string_view>();
        test_copy_assignment<char const*>();
    }

BOOST_AUTO_TEST_SUITE_END() // byte_sequence_view_

} // namespace whatwg::infra::testing

