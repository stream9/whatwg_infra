#include <whatwg/infra/code_point.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::infra::testing {

BOOST_AUTO_TEST_SUITE(code_point_)

    BOOST_AUTO_TEST_CASE(is_valid_code_point_)
    {
        BOOST_CHECK(is_valid_code_point(U'\x00'));
        BOOST_CHECK(is_valid_code_point(U'\x10FFFF'));
        BOOST_CHECK(!is_valid_code_point(U'\x110000'));
        BOOST_CHECK(!is_valid_code_point(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_surrogate_)
    {
        BOOST_CHECK(!is_surrogate(U'\x00'));
        BOOST_CHECK(!is_surrogate(U'\xD7FF'));
        BOOST_CHECK(is_surrogate(U'\xD800'));
        BOOST_CHECK(is_surrogate(U'\xDFFF'));
        BOOST_CHECK(!is_surrogate(U'\xE000'));
        BOOST_CHECK(!is_surrogate(U'\x10FFFF'));
        BOOST_CHECK(!is_surrogate(U'\x110000'));
        BOOST_CHECK(!is_surrogate(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_scalar_value_)
    {
        BOOST_CHECK(is_scalar_value(U'\x00'));
        BOOST_CHECK(is_scalar_value(U'\xD7FF'));
        BOOST_CHECK(!is_scalar_value(U'\xD800'));
        BOOST_CHECK(!is_scalar_value(U'\xDFFF'));
        BOOST_CHECK(is_scalar_value(U'\xE000'));
        BOOST_CHECK(is_scalar_value(U'\x10FFFF'));
        BOOST_CHECK(!is_scalar_value(U'\x110000'));
        BOOST_CHECK(!is_scalar_value(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_noncharacter_)
    {
        BOOST_CHECK(!is_noncharacter(U'\x00'));
        BOOST_CHECK(!is_noncharacter(U'\xFDCF'));
        BOOST_CHECK( is_noncharacter(U'\xFDD0'));
        BOOST_CHECK( is_noncharacter(U'\xFDEF'));
        BOOST_CHECK(!is_noncharacter(U'\xFDF0'));
        BOOST_CHECK(!is_noncharacter(U'\xFFFD'));
        BOOST_CHECK( is_noncharacter(U'\xFFFE'));
        BOOST_CHECK( is_noncharacter(U'\xFFFF'));
        BOOST_CHECK(!is_noncharacter(U'\x10000'));
        BOOST_CHECK(!is_noncharacter(U'\x1FFFD'));
        BOOST_CHECK( is_noncharacter(U'\x1FFFE'));
        BOOST_CHECK( is_noncharacter(U'\x1FFFF'));
        BOOST_CHECK(!is_noncharacter(U'\x20000'));
        BOOST_CHECK(!is_noncharacter(U'\x2FFFD'));
        BOOST_CHECK( is_noncharacter(U'\x2FFFE'));
        BOOST_CHECK( is_noncharacter(U'\x2FFFF'));
        BOOST_CHECK(!is_noncharacter(U'\x30000'));
        BOOST_CHECK(!is_noncharacter(U'\x3FFFD'));
        BOOST_CHECK( is_noncharacter(U'\x3FFFE'));
        BOOST_CHECK( is_noncharacter(U'\x3FFFF'));
        BOOST_CHECK(!is_noncharacter(U'\x40000'));
        BOOST_CHECK(!is_noncharacter(U'\x4FFFD'));
        BOOST_CHECK( is_noncharacter(U'\x4FFFE'));
        BOOST_CHECK( is_noncharacter(U'\x4FFFF'));
        BOOST_CHECK(!is_noncharacter(U'\x50000'));
        BOOST_CHECK(!is_noncharacter(U'\x5FFFD'));
        BOOST_CHECK( is_noncharacter(U'\x5FFFE'));
        BOOST_CHECK( is_noncharacter(U'\x5FFFF'));
        BOOST_CHECK(!is_noncharacter(U'\x60000'));
        BOOST_CHECK(!is_noncharacter(U'\x6FFFD'));
        BOOST_CHECK( is_noncharacter(U'\x6FFFE'));
        BOOST_CHECK( is_noncharacter(U'\x6FFFF'));
        BOOST_CHECK(!is_noncharacter(U'\x70000'));
        BOOST_CHECK(!is_noncharacter(U'\x7FFFD'));
        BOOST_CHECK( is_noncharacter(U'\x7FFFE'));
        BOOST_CHECK( is_noncharacter(U'\x7FFFF'));
        BOOST_CHECK(!is_noncharacter(U'\x80000'));
        BOOST_CHECK(!is_noncharacter(U'\x8FFFD'));
        BOOST_CHECK( is_noncharacter(U'\x8FFFE'));
        BOOST_CHECK( is_noncharacter(U'\x8FFFF'));
        BOOST_CHECK(!is_noncharacter(U'\x90000'));
        BOOST_CHECK(!is_noncharacter(U'\x9FFFD'));
        BOOST_CHECK( is_noncharacter(U'\x9FFFE'));
        BOOST_CHECK( is_noncharacter(U'\x9FFFF'));
        BOOST_CHECK(!is_noncharacter(U'\xA0000'));
        BOOST_CHECK(!is_noncharacter(U'\xAFFFD'));
        BOOST_CHECK( is_noncharacter(U'\xAFFFE'));
        BOOST_CHECK( is_noncharacter(U'\xAFFFF'));
        BOOST_CHECK(!is_noncharacter(U'\xB0000'));
        BOOST_CHECK(!is_noncharacter(U'\xBFFFD'));
        BOOST_CHECK( is_noncharacter(U'\xBFFFE'));
        BOOST_CHECK( is_noncharacter(U'\xBFFFF'));
        BOOST_CHECK(!is_noncharacter(U'\xC0000'));
        BOOST_CHECK(!is_noncharacter(U'\xCFFFD'));
        BOOST_CHECK( is_noncharacter(U'\xCFFFE'));
        BOOST_CHECK( is_noncharacter(U'\xCFFFF'));
        BOOST_CHECK(!is_noncharacter(U'\xD0000'));
        BOOST_CHECK(!is_noncharacter(U'\xDFFFD'));
        BOOST_CHECK( is_noncharacter(U'\xDFFFE'));
        BOOST_CHECK( is_noncharacter(U'\xDFFFF'));
        BOOST_CHECK(!is_noncharacter(U'\xE0000'));
        BOOST_CHECK(!is_noncharacter(U'\xEFFFD'));
        BOOST_CHECK( is_noncharacter(U'\xEFFFE'));
        BOOST_CHECK( is_noncharacter(U'\xEFFFF'));
        BOOST_CHECK(!is_noncharacter(U'\xF0000'));
        BOOST_CHECK(!is_noncharacter(U'\xFFFFD'));
        BOOST_CHECK( is_noncharacter(U'\xFFFFE'));
        BOOST_CHECK( is_noncharacter(U'\xFFFFF'));
        BOOST_CHECK(!is_noncharacter(U'\x100000'));
        BOOST_CHECK(!is_noncharacter(U'\x10FFFD'));
        BOOST_CHECK( is_noncharacter(U'\x10FFFE'));
        BOOST_CHECK( is_noncharacter(U'\x10FFFF'));
        BOOST_CHECK(!is_noncharacter(U'\x110000'));
        BOOST_CHECK(!is_noncharacter(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_code_point_)
    {
        BOOST_CHECK( is_ascii_code_point(U'\x00'));
        BOOST_CHECK( is_ascii_code_point(U'\x7F'));
        BOOST_CHECK(!is_ascii_code_point(U'\x80'));
        BOOST_CHECK(!is_ascii_code_point(U'\x10FFFF'));
        BOOST_CHECK(!is_ascii_code_point(U'\x110000'));
        BOOST_CHECK(!is_ascii_code_point(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_tab_or_newline_)
    {
        BOOST_CHECK(!is_ascii_tab_or_newline(U'\x00'));
        BOOST_CHECK(!is_ascii_tab_or_newline(U'\x08'));
        BOOST_CHECK( is_ascii_tab_or_newline(U'\x09'));
        BOOST_CHECK( is_ascii_tab_or_newline(U'\x0A'));
        BOOST_CHECK(!is_ascii_tab_or_newline(U'\x0B'));
        BOOST_CHECK(!is_ascii_tab_or_newline(U'\x0C'));
        BOOST_CHECK( is_ascii_tab_or_newline(U'\x0D'));
        BOOST_CHECK(!is_ascii_tab_or_newline(U'\x0E'));
        BOOST_CHECK(!is_ascii_tab_or_newline(U'\x10FFFF'));
        BOOST_CHECK(!is_ascii_tab_or_newline(U'\x110000'));
        BOOST_CHECK(!is_ascii_tab_or_newline(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_whitespace_)
    {
        BOOST_CHECK(!is_ascii_whitespace(U'\x00'));
        BOOST_CHECK(!is_ascii_whitespace(U'\x08'));
        BOOST_CHECK( is_ascii_whitespace(U'\x09'));
        BOOST_CHECK( is_ascii_whitespace(U'\x0A'));
        BOOST_CHECK(!is_ascii_whitespace(U'\x0B'));
        BOOST_CHECK( is_ascii_whitespace(U'\x0C'));
        BOOST_CHECK( is_ascii_whitespace(U'\x0D'));
        BOOST_CHECK(!is_ascii_whitespace(U'\x0E'));
        BOOST_CHECK(!is_ascii_whitespace(U'\x1F'));
        BOOST_CHECK( is_ascii_whitespace(U'\x20'));
        BOOST_CHECK(!is_ascii_whitespace(U'\x21'));
        BOOST_CHECK(!is_ascii_whitespace(U'\x10FFFF'));
        BOOST_CHECK(!is_ascii_whitespace(U'\x110000'));
        BOOST_CHECK(!is_ascii_whitespace(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_c0_control_)
    {
        BOOST_CHECK( is_c0_control(U'\x00'));
        BOOST_CHECK( is_c0_control(U'\x1F'));
        BOOST_CHECK(!is_c0_control(U'\x20'));
        BOOST_CHECK(!is_c0_control(U'\x10FFFF'));
        BOOST_CHECK(!is_c0_control(U'\x110000'));
        BOOST_CHECK(!is_c0_control(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_c0_control_or_space_)
    {
        BOOST_CHECK( is_c0_control_or_space(U'\x00'));
        BOOST_CHECK( is_c0_control_or_space(U'\x1F'));
        BOOST_CHECK( is_c0_control_or_space(U'\x20'));
        BOOST_CHECK(!is_c0_control_or_space(U'\x21'));
        BOOST_CHECK(!is_c0_control_or_space(U'\x10FFFF'));
        BOOST_CHECK(!is_c0_control_or_space(U'\x110000'));
        BOOST_CHECK(!is_c0_control_or_space(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_control_)
    {
        BOOST_CHECK( is_control(U'\x00'));
        BOOST_CHECK( is_control(U'\x1F'));
        BOOST_CHECK(!is_control(U'\x20'));
        BOOST_CHECK(!is_control(U'\x7E'));
        BOOST_CHECK( is_control(U'\x7F'));
        BOOST_CHECK( is_control(U'\x9F'));
        BOOST_CHECK(!is_control(U'\xA0'));
        BOOST_CHECK(!is_control(U'\x10FFFF'));
        BOOST_CHECK(!is_control(U'\x110000'));
        BOOST_CHECK(!is_control(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_digit_)
    {
        BOOST_CHECK(!is_ascii_digit(U'\x00'));
        BOOST_CHECK(!is_ascii_digit(U'\x2F'));
        BOOST_CHECK( is_ascii_digit(U'\x30'));
        BOOST_CHECK( is_ascii_digit(U'\x39'));
        BOOST_CHECK(!is_ascii_digit(U'\x3A'));
        BOOST_CHECK(!is_ascii_digit(U'\x10FFFF'));
        BOOST_CHECK(!is_ascii_digit(U'\x110000'));
        BOOST_CHECK(!is_ascii_digit(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_upper_hex_digit_)
    {
        BOOST_CHECK(!is_ascii_upper_hex_digit(U'\x00'));
        BOOST_CHECK(!is_ascii_upper_hex_digit(U'\x2F'));
        BOOST_CHECK( is_ascii_upper_hex_digit(U'\x30'));
        BOOST_CHECK( is_ascii_upper_hex_digit(U'\x39'));
        BOOST_CHECK(!is_ascii_upper_hex_digit(U'\x3A'));
        BOOST_CHECK(!is_ascii_upper_hex_digit(U'\x40'));
        BOOST_CHECK( is_ascii_upper_hex_digit(U'\x41'));
        BOOST_CHECK( is_ascii_upper_hex_digit(U'\x46'));
        BOOST_CHECK(!is_ascii_upper_hex_digit(U'\x47'));
        BOOST_CHECK(!is_ascii_upper_hex_digit(U'\x10FFFF'));
        BOOST_CHECK(!is_ascii_upper_hex_digit(U'\x110000'));
        BOOST_CHECK(!is_ascii_upper_hex_digit(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_lower_hex_digit_)
    {
        BOOST_CHECK(!is_ascii_lower_hex_digit(U'\x00'));
        BOOST_CHECK(!is_ascii_lower_hex_digit(U'\x2F'));
        BOOST_CHECK( is_ascii_lower_hex_digit(U'\x30'));
        BOOST_CHECK( is_ascii_lower_hex_digit(U'\x39'));
        BOOST_CHECK(!is_ascii_lower_hex_digit(U'\x3A'));
        BOOST_CHECK(!is_ascii_lower_hex_digit(U'\x60'));
        BOOST_CHECK( is_ascii_lower_hex_digit(U'\x61'));
        BOOST_CHECK( is_ascii_lower_hex_digit(U'\x66'));
        BOOST_CHECK(!is_ascii_lower_hex_digit(U'\x67'));
        BOOST_CHECK(!is_ascii_lower_hex_digit(U'\x10FFFF'));
        BOOST_CHECK(!is_ascii_lower_hex_digit(U'\x110000'));
        BOOST_CHECK(!is_ascii_lower_hex_digit(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_hex_digit_)
    {
        BOOST_CHECK(!is_ascii_hex_digit(U'\x00'));
        BOOST_CHECK(!is_ascii_hex_digit(U'\x2F'));
        BOOST_CHECK( is_ascii_hex_digit(U'\x30'));
        BOOST_CHECK( is_ascii_hex_digit(U'\x39'));
        BOOST_CHECK(!is_ascii_hex_digit(U'\x3A'));
        BOOST_CHECK(!is_ascii_hex_digit(U'\x40'));
        BOOST_CHECK( is_ascii_hex_digit(U'\x41'));
        BOOST_CHECK( is_ascii_hex_digit(U'\x46'));
        BOOST_CHECK(!is_ascii_hex_digit(U'\x47'));
        BOOST_CHECK(!is_ascii_hex_digit(U'\x60'));
        BOOST_CHECK( is_ascii_hex_digit(U'\x61'));
        BOOST_CHECK( is_ascii_hex_digit(U'\x66'));
        BOOST_CHECK(!is_ascii_hex_digit(U'\x67'));
        BOOST_CHECK(!is_ascii_hex_digit(U'\x10FFFF'));
        BOOST_CHECK(!is_ascii_hex_digit(U'\x110000'));
        BOOST_CHECK(!is_ascii_hex_digit(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_upper_alpha_)
    {
        BOOST_CHECK(!is_ascii_upper_alpha(U'\x00'));
        BOOST_CHECK(!is_ascii_upper_alpha(U'\x40'));
        BOOST_CHECK( is_ascii_upper_alpha(U'\x41'));
        BOOST_CHECK( is_ascii_upper_alpha(U'\x5A'));
        BOOST_CHECK(!is_ascii_upper_alpha(U'\x5B'));
        BOOST_CHECK(!is_ascii_upper_alpha(U'\x10FFFF'));
        BOOST_CHECK(!is_ascii_upper_alpha(U'\x110000'));
        BOOST_CHECK(!is_ascii_upper_alpha(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_lower_alpha_)
    {
        BOOST_CHECK(!is_ascii_lower_alpha(U'\x00'));
        BOOST_CHECK(!is_ascii_lower_alpha(U'\x60'));
        BOOST_CHECK( is_ascii_lower_alpha(U'\x61'));
        BOOST_CHECK( is_ascii_lower_alpha(U'\x7A'));
        BOOST_CHECK(!is_ascii_lower_alpha(U'\x7B'));
        BOOST_CHECK(!is_ascii_lower_alpha(U'\x10FFFF'));
        BOOST_CHECK(!is_ascii_lower_alpha(U'\x110000'));
        BOOST_CHECK(!is_ascii_lower_alpha(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(to_ascii_upper_)
    {
        BOOST_CHECK_EQUAL(to_ascii_upper(U'\x00'), U'\x00');
        BOOST_CHECK_EQUAL(to_ascii_upper(U'\x40'), U'\x40');
        BOOST_CHECK_EQUAL(to_ascii_upper(U'\x41'), U'\x41');
        BOOST_CHECK_EQUAL(to_ascii_upper(U'\x5A'), U'\x5A');
        BOOST_CHECK_EQUAL(to_ascii_upper(U'\x5B'), U'\x5B');
        BOOST_CHECK_EQUAL(to_ascii_upper(U'\x60'), U'\x60');
        BOOST_CHECK_EQUAL(to_ascii_upper(U'\x61'), U'\x41');
        BOOST_CHECK_EQUAL(to_ascii_upper(U'\x7A'), U'\x5A');
        BOOST_CHECK_EQUAL(to_ascii_upper(U'\x7B'), U'\x7B');
        BOOST_CHECK_EQUAL(to_ascii_upper(U'\x10FFFF'), U'\x10FFFF');
        BOOST_CHECK_EQUAL(to_ascii_upper(U'\x110000'), U'\x110000');
        BOOST_CHECK_EQUAL(to_ascii_upper(U'\xFFFFFFFF'), U'\xFFFFFFFF');
    }

    BOOST_AUTO_TEST_CASE(to_ascii_lower_)
    {
        BOOST_CHECK_EQUAL(to_ascii_lower(U'\x00'), U'\x00');
        BOOST_CHECK_EQUAL(to_ascii_lower(U'\x40'), U'\x40');
        BOOST_CHECK_EQUAL(to_ascii_lower(U'\x41'), U'\x61');
        BOOST_CHECK_EQUAL(to_ascii_lower(U'\x5A'), U'\x7A');
        BOOST_CHECK_EQUAL(to_ascii_lower(U'\x5B'), U'\x5B');
        BOOST_CHECK_EQUAL(to_ascii_lower(U'\x60'), U'\x60');
        BOOST_CHECK_EQUAL(to_ascii_lower(U'\x61'), U'\x61');
        BOOST_CHECK_EQUAL(to_ascii_lower(U'\x7A'), U'\x7A');
        BOOST_CHECK_EQUAL(to_ascii_lower(U'\x7B'), U'\x7B');
        BOOST_CHECK_EQUAL(to_ascii_lower(U'\x10FFFF'), U'\x10FFFF');
        BOOST_CHECK_EQUAL(to_ascii_lower(U'\x110000'), U'\x110000');
        BOOST_CHECK_EQUAL(to_ascii_lower(U'\xFFFFFFFF'), U'\xFFFFFFFF');
    }

    BOOST_AUTO_TEST_CASE(is_ascii_alpha_)
    {
        BOOST_CHECK(!is_ascii_alpha(U'\x00'));
        BOOST_CHECK(!is_ascii_alpha(U'\x40'));
        BOOST_CHECK( is_ascii_alpha(U'\x41'));
        BOOST_CHECK( is_ascii_alpha(U'\x5A'));
        BOOST_CHECK(!is_ascii_alpha(U'\x5B'));
        BOOST_CHECK(!is_ascii_alpha(U'\x60'));
        BOOST_CHECK( is_ascii_alpha(U'\x61'));
        BOOST_CHECK( is_ascii_alpha(U'\x7A'));
        BOOST_CHECK(!is_ascii_alpha(U'\x7B'));
        BOOST_CHECK(!is_ascii_alpha(U'\x10FFFF'));
        BOOST_CHECK(!is_ascii_alpha(U'\x110000'));
        BOOST_CHECK(!is_ascii_alpha(U'\xFFFFFFFF'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_alphanumeric_)
    {
        BOOST_CHECK(!is_ascii_alphanumeric(U'\x00'));
        BOOST_CHECK(!is_ascii_alphanumeric(U'\x2F'));
        BOOST_CHECK( is_ascii_alphanumeric(U'\x30'));
        BOOST_CHECK( is_ascii_alphanumeric(U'\x39'));
        BOOST_CHECK(!is_ascii_alphanumeric(U'\x3A'));
        BOOST_CHECK(!is_ascii_alphanumeric(U'\x40'));
        BOOST_CHECK( is_ascii_alphanumeric(U'\x41'));
        BOOST_CHECK( is_ascii_alphanumeric(U'\x5A'));
        BOOST_CHECK(!is_ascii_alphanumeric(U'\x5B'));
        BOOST_CHECK(!is_ascii_alphanumeric(U'\x60'));
        BOOST_CHECK( is_ascii_alphanumeric(U'\x61'));
        BOOST_CHECK( is_ascii_alphanumeric(U'\x7A'));
        BOOST_CHECK(!is_ascii_alphanumeric(U'\x7B'));
        BOOST_CHECK(!is_ascii_alphanumeric(U'\x10FFFF'));
        BOOST_CHECK(!is_ascii_alphanumeric(U'\x110000'));
        BOOST_CHECK(!is_ascii_alphanumeric(U'\xFFFFFFFF'));
    }

BOOST_AUTO_TEST_SUITE_END() // code_point_

} // namespace whatwg::infra::testing

