#include <whatwg/infra/string.hpp>

#include <whatwg/infra/ostream.hpp>

#include <string>

#include <boost/test/unit_test.hpp>

namespace whatwg::infra::testing {

BOOST_AUTO_TEST_SUITE(string_)

    BOOST_AUTO_TEST_CASE(append_code_point)
    {
        string_t s = U"A";
        code_point_t c = U'B';
        append(s, c);

        BOOST_CHECK(s == U"AB");
    }

    BOOST_AUTO_TEST_CASE(append_string_view)
    {
        string_t s1 = U"A";
        string_view_t const s2 = U"BC";
        append(s1, s2);

        BOOST_CHECK(s1 == U"ABC");
    }

    BOOST_AUTO_TEST_CASE(prepend_code_point)
    {
        string_t s;

        prepend(s, U'a');
        BOOST_CHECK_EQUAL(s, U"a");

        prepend(s, '\0');
        BOOST_CHECK_EQUAL(s, U"\0a"_sv);
    }

    BOOST_AUTO_TEST_CASE(prepend_view)
    {
        string_t s;

        prepend(s, U"ABC"_sv);
        BOOST_CHECK_EQUAL(s, U"ABC"_sv);

        prepend(s, U""_sv);
        BOOST_CHECK_EQUAL(s, U"ABC"_sv);

        prepend(s, U"\0"_sv);
        BOOST_CHECK_EQUAL(s, U"\0ABC"_sv);
    }

    BOOST_AUTO_TEST_CASE(length_)
    {
        string_t const s1;
        BOOST_CHECK_EQUAL(length(s1), 0);

        string_t const s2 = U"foo";
        BOOST_CHECK_EQUAL(length(s2), 3);
    }

    BOOST_AUTO_TEST_CASE(is_empty_)
    {
        string_t s;
        BOOST_CHECK(is_empty(s));

        append(s, U"ABC");
        BOOST_CHECK(!is_empty(s));
    }

    BOOST_AUTO_TEST_CASE(isomorphic_encode_)
    {
        string_t const s1;
        BOOST_CHECK_EQUAL(isomorphic_encode(s1), "");

        string_t const s2 = U"ABC";
        BOOST_CHECK_EQUAL(isomorphic_encode(s2), "ABC");
    }

    BOOST_AUTO_TEST_CASE(is_ascii_string_)
    {
        string_t const s1;
        BOOST_CHECK(is_ascii_string(s1));

        string_t const s2 = U"ABC";
        BOOST_CHECK(is_ascii_string(s2));

        string_t const s3 = U"\x80\x81\x82";
        BOOST_CHECK(!is_ascii_string(s3));

        string_t const s4 = U"\x10FFFD\x10FFFE\x10FFFF";
        BOOST_CHECK(!is_ascii_string(s4));

        string_t const s5 = U"\x110000\x110001\x110002";
        BOOST_CHECK(!is_ascii_string(s5));

        string_t const s6 = U"ABC\x80";
        BOOST_CHECK(!is_ascii_string(s6));
    }

    BOOST_AUTO_TEST_CASE(ascii_lowercase_)
    {
        string_t s = U"\x01\x1F ABCabc123\x80\xFF";

        ascii_lowercase(s);
        BOOST_CHECK(s == U"\x01\x1F abcabc123\x80\xFF");
    }

    BOOST_AUTO_TEST_CASE(ascii_lowercase_copy_)
    {
        string_t const s1 = U"\x01\x1F ABCabc123\x80\xFF";

        auto const s2 = ascii_lowercase_copy(s1);
        BOOST_CHECK(s2 == U"\x01\x1F abcabc123\x80\xFF");
    }

    BOOST_AUTO_TEST_CASE(ascii_uppercase_)
    {
        string_t s = U"\x01\x1F ABCabc123\x80\xFF";

        ascii_uppercase(s);
        BOOST_CHECK(s == U"\x01\x1F ABCABC123\x80\xFF");
    }

    BOOST_AUTO_TEST_CASE(ascii_uppercase_copy_)
    {
        string_t const s1 = U"\x01\x1F ABCabc123\x80\xFF";

        auto const s2 = ascii_uppercase_copy(s1);
        BOOST_CHECK(s2 == U"\x01\x1F ABCABC123\x80\xFF");
    }

    BOOST_AUTO_TEST_CASE(ascii_case_insensitive_match_1)
    {
        string_view_t const s1;
        string_view_t const s2;

        BOOST_CHECK(ascii_case_insensitive_match(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(ascii_case_insensitive_match_2)
    {
        string_view_t const s1 = U"abcDEF";
        string_view_t const s2 = U"ABCdef";

        BOOST_CHECK(ascii_case_insensitive_match(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(ascii_case_insensitive_match_3)
    {
        string_view_t const s1 = U"abcDEF";
        string_view_t const s2 = U"ABCdefg";

        BOOST_CHECK(!ascii_case_insensitive_match(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(ascii_encode_)
    {
        BOOST_CHECK(ascii_encode(U"") == "");
        BOOST_CHECK(ascii_encode(U"ABC") == "ABC");
    }

    BOOST_AUTO_TEST_CASE(ascii_decode_)
    {
        BOOST_CHECK(ascii_decode("") == U"");
        BOOST_CHECK(ascii_decode("ABC") == U"ABC");
    }

    BOOST_AUTO_TEST_CASE(strip_newlines_)
    {
        string_t s1;
        strip_newlines(s1);
        BOOST_CHECK_EQUAL(s1, U"");

        string_t s2 = U"\n1\r\n2\n3\n\n";
        strip_newlines(s2);
        BOOST_CHECK_EQUAL(s2, U"123");

        string_t s3 = U"\n\r\n\n\r\n";
        strip_newlines(s3);
        BOOST_CHECK_EQUAL(s3, U"");
    }

    BOOST_AUTO_TEST_CASE(strip_leading_and_trailing_ascii_whitespace_string)
    {
        string_t s1;
        strip_leading_and_trailing_ascii_whitespace(s1);
        BOOST_CHECK_EQUAL(s1, U"");

        string_t s2 = U"    123 456  ";
        strip_leading_and_trailing_ascii_whitespace(s2);
        BOOST_CHECK_EQUAL(s2, U"123 456");

        string_t s3 = U"\r\n\t123\n\n\t456\r\r\f";
        strip_leading_and_trailing_ascii_whitespace(s3);
        BOOST_CHECK_EQUAL(s3, U"123\n\n\t456");
    }

    BOOST_AUTO_TEST_CASE(strip_leading_and_trailing_ascii_whitespace_string_view)
    {
        string_view_t s1;
        strip_leading_and_trailing_ascii_whitespace(s1);
        BOOST_CHECK_EQUAL(s1, U"");

        string_view_t s2 = U"    123 456  ";
        strip_leading_and_trailing_ascii_whitespace(s2);
        BOOST_CHECK_EQUAL(s2, U"123 456");

        string_view_t s3 = U"\r\n\t123\n\n\t456\r\r\f";
        strip_leading_and_trailing_ascii_whitespace(s3);
        BOOST_CHECK_EQUAL(s3, U"123\n\n\t456");
    }

    BOOST_AUTO_TEST_CASE(strip_and_collapse_ascii_whitespace_)
    {
        string_t s1;
        strip_and_collapse_ascii_whitespace(s1);
        BOOST_CHECK_EQUAL(s1, U"");

        string_t s2 = U"    123   456  ";
        strip_and_collapse_ascii_whitespace(s2);
        BOOST_CHECK_EQUAL(s2, U"123 456");

        string_t s3 = U"\r\n\t123\n\n\t456\r\r\f";
        strip_and_collapse_ascii_whitespace(s3);
        BOOST_CHECK_EQUAL(s3, U"123 456");
    }

    BOOST_AUTO_TEST_CASE(collect_a_sequence_of_code_point_0)
    {
        string_t const input = U"";
        position_t pos = 0;

        auto pred = static_cast<bool(*)(code_point_t)>(is_ascii_alpha);

        auto const output = collect_a_sequence_of_code_point(
            input, pos, pred);

        BOOST_CHECK_EQUAL(output, U"");
        BOOST_CHECK_EQUAL(pos, 0);
    }

    BOOST_AUTO_TEST_CASE(collect_a_sequence_of_code_point_1)
    {
        string_t const input = U"ABC123def";
        position_t pos = 0;

        auto pred = static_cast<bool(*)(code_point_t)>(is_ascii_alpha);

        auto const output = collect_a_sequence_of_code_point(
            input, pos, pred);

        BOOST_CHECK_EQUAL(output, U"ABC");
        BOOST_CHECK_EQUAL(pos, 3);
    }

    BOOST_AUTO_TEST_CASE(collect_a_sequence_of_code_point_2)
    {
        string_t const input = U"ABC123def";
        position_t pos = 3;

        auto pred = static_cast<bool(*)(code_point_t)>(is_ascii_alpha);

        auto const output = collect_a_sequence_of_code_point(
            input, pos, pred);

        BOOST_CHECK_EQUAL(output, U"");
        BOOST_CHECK_EQUAL(pos, 3);
    }

    BOOST_AUTO_TEST_CASE(collect_a_sequence_of_code_point_3)
    {
        string_t const input = U"ABC123def";
        position_t pos = 7;

        auto pred = static_cast<bool(*)(code_point_t)>(is_ascii_alpha);

        auto const output = collect_a_sequence_of_code_point(
            input, pos, pred);

        BOOST_CHECK_EQUAL(output, U"ef");
        BOOST_CHECK_EQUAL(pos, 9);
    }

    BOOST_AUTO_TEST_CASE(collect_a_sequence_of_code_point_4)
    {
        string_t const input = U"ABC123def";
        position_t pos = 9;

        auto pred = static_cast<bool(*)(code_point_t)>(is_ascii_alpha);

        auto const output = collect_a_sequence_of_code_point(
            input, pos, pred);

        BOOST_CHECK_EQUAL(output, U"");
        BOOST_CHECK_EQUAL(pos, 9);
    }

    BOOST_AUTO_TEST_CASE(collect_a_sequence_of_code_point_5)
    {
        string_t const input = U"ABC123def";
        position_t pos = 500;

        auto pred = static_cast<bool(*)(code_point_t)>(is_ascii_alpha);

        auto const output = collect_a_sequence_of_code_point(
            input, pos, pred);

        BOOST_CHECK_EQUAL(output, U"");
        BOOST_CHECK_EQUAL(pos, 500);
    }

    BOOST_AUTO_TEST_CASE(skip_ascii_whitespace_0)
    {
        string_t const input = U"";
        position_t pos = 0;

        skip_ascii_whitespace(input, pos);

        BOOST_CHECK_EQUAL(pos, 0);
    }

    BOOST_AUTO_TEST_CASE(skip_ascii_whitespace_1)
    {
        string_t const input = U"   ABC   DEF   ";

        position_t pos = 0;
        skip_ascii_whitespace(input, pos);
        BOOST_CHECK_EQUAL(pos, 3);

        skip_ascii_whitespace(input, pos);
        BOOST_CHECK_EQUAL(pos, 3);

        pos = 7;
        skip_ascii_whitespace(input, pos);
        BOOST_CHECK_EQUAL(pos, 9);

        pos = 16;
        skip_ascii_whitespace(input, pos);
        BOOST_CHECK_EQUAL(pos, 16);

        pos = 100;
        skip_ascii_whitespace(input, pos);
        BOOST_CHECK_EQUAL(pos, 100);
    }

    BOOST_AUTO_TEST_CASE(strictly_split_0)
    {
        auto const tokens = strictly_split(U"", U',');
        BOOST_REQUIRE_EQUAL(size(tokens), 1);
        BOOST_CHECK_EQUAL(tokens[0], U"");
    }

    BOOST_AUTO_TEST_CASE(strictly_split_1)
    {
        string_view_t const input = U" ABC, DEF, GHI ";
        auto const tokens = strictly_split(input, U',');
        BOOST_REQUIRE_EQUAL(size(tokens), 3);
        BOOST_CHECK_EQUAL(tokens[0], U" ABC");
        BOOST_CHECK_EQUAL(tokens[1], U" DEF");
        BOOST_CHECK_EQUAL(tokens[2], U" GHI ");
    }

    BOOST_AUTO_TEST_CASE(strictly_split_2)
    {
        string_view_t const input = U" ABC, DEF, GHI ";
        auto const tokens = strictly_split(input, U'\t');
        BOOST_REQUIRE_EQUAL(size(tokens), 1);
        BOOST_CHECK_EQUAL(tokens[0], U" ABC, DEF, GHI ");
    }

    BOOST_AUTO_TEST_CASE(strictly_split_3)
    {
        string_view_t const input = U" ABC, DEF, GHI ";
        auto const tokens = strictly_split(input, U' ');
        BOOST_REQUIRE_EQUAL(size(tokens), 5);
        BOOST_CHECK_EQUAL(tokens[0], U"");
        BOOST_CHECK_EQUAL(tokens[1], U"ABC,");
        BOOST_CHECK_EQUAL(tokens[2], U"DEF,");
        BOOST_CHECK_EQUAL(tokens[3], U"GHI");
        BOOST_CHECK_EQUAL(tokens[4], U"");
    }

    BOOST_AUTO_TEST_CASE(split_on_ascii_whitespace_0)
    {
        auto const tokens = split_on_ascii_whitespace(U"");
        BOOST_REQUIRE_EQUAL(size(tokens), 0);
    }

    BOOST_AUTO_TEST_CASE(split_on_ascii_whitespace_1)
    {
        string_view_t const input = U"  ABC  DEF  GHI  ";
        auto const tokens = split_on_ascii_whitespace(input);
        BOOST_REQUIRE_EQUAL(size(tokens), 3);
        BOOST_CHECK_EQUAL(tokens[0], U"ABC");
        BOOST_CHECK_EQUAL(tokens[1], U"DEF");
        BOOST_CHECK_EQUAL(tokens[2], U"GHI");
    }

    BOOST_AUTO_TEST_CASE(split_on_commas_0)
    {
        auto const tokens = split_on_commas(U"");
        BOOST_REQUIRE_EQUAL(size(tokens), 0);
    }

    BOOST_AUTO_TEST_CASE(split_on_commas_1)
    {
        string_view_t const input = U"  ABC, DEF, GHI  ";
        auto const tokens = split_on_commas(input);
        BOOST_REQUIRE_EQUAL(size(tokens), 3);
        BOOST_CHECK_EQUAL(tokens[0], U"ABC");
        BOOST_CHECK_EQUAL(tokens[1], U"DEF");
        BOOST_CHECK_EQUAL(tokens[2], U"GHI");
    }

    BOOST_AUTO_TEST_CASE(concatenate_0)
    {
        list<string_view_t> strings;
        auto const r = concatenate(strings, U",");
        BOOST_CHECK_EQUAL(r, U"");
    }

    BOOST_AUTO_TEST_CASE(concatenate_1)
    {
        list<string_view_t> const strings = {
            U"ABC", U"DEF", U"GHI"
        };

        auto const r = concatenate(strings);
        BOOST_CHECK_EQUAL(r, U"ABCDEFGHI");
    }

    BOOST_AUTO_TEST_CASE(concatenate_2)
    {
        list<string_view_t> const strings = {
            U"ABC", U"DEF", U"GHI"
        };

        auto const r = concatenate(strings, U", ");
        BOOST_CHECK_EQUAL(r, U"ABC, DEF, GHI, ");
    }

    BOOST_AUTO_TEST_CASE(from_std_string_to_string)
    {
        std::basic_string<char32_t> s1 = U"FOO";

        string_t const s2 { s1 };
        BOOST_CHECK(s2 == s1);

        string_t s3;
        s3 = s1;
        BOOST_CHECK(s3 == s1);

        string_t const s4 { std::move(s1) };
        BOOST_CHECK(s4 == s2);

        s1 = U"FOO";
        string_t s5;
        s5 = std::move(s1);
        BOOST_CHECK(s5 == s2);
    }

    BOOST_AUTO_TEST_CASE(from_string_to_std_string)
    {
        using std_string = std::basic_string<char32_t>;

        string_t s1 = U"FOO";

        std_string const s2 { s1 };
        BOOST_CHECK(s2 == s1);

        std_string s3;
        s3 = s1;
        BOOST_CHECK(s3 == s1);

        std_string const s4 { std::move(s1) };
        BOOST_CHECK(s4 == s2);

        s1 = U"FOO";
        std_string s5;
        s5 = std::move(s1);
        BOOST_TEST_INFO(s5.size());
        BOOST_CHECK(s5 == s2);
    }

    BOOST_AUTO_TEST_CASE(from_literal_to_string)
    {
        string_t const s2 { U"FOO" };
        BOOST_CHECK(s2 == U"FOO");

        string_t s3;
        s3 = U"FOO";
        BOOST_CHECK(s3 == U"FOO");
    }

    BOOST_AUTO_TEST_CASE(from_string_view_to_string)
    {
        string_view_t s1 = U"FOO";

        string_t const s2 { s1 };
        BOOST_CHECK(s2 == s1);

        string_t s3;
        s3 = s1;
        BOOST_CHECK(s3 == s1);
    }

    BOOST_AUTO_TEST_CASE(from_std_string_view_to_string)
    {
        using std_string_view = std::basic_string_view<char32_t>;
        std_string_view s1 = U"FOO";

        string_t const s2 { s1 };
        BOOST_CHECK(s2 == s1);

        string_t s3;
        s3 = s1;
        BOOST_CHECK(s3 == s1);
    }

    BOOST_AUTO_TEST_CASE(from_std_string_view_to_string_view)
    {
        using std_string_view = std::basic_string_view<char32_t>;
        std_string_view s1 = U"FOO";

        static_assert(std::is_same_v<decltype(s1), std::basic_string_view<code_point_t>>);

        string_view_t const s2 { s1 };
        BOOST_CHECK(s2 == s1);

        string_view_t s3;
        s3 = s1;
        BOOST_CHECK(s3 == s1);
    }

    BOOST_AUTO_TEST_CASE(from_std_string_to_string_view)
    {
        using std_string = std::basic_string<char32_t>;
        std_string s1 = U"FOO";

        string_view_t const s2 { s1 };
        BOOST_CHECK(s2 == s1);

        string_view_t s3;
        s3 = s1;
        BOOST_CHECK(s3 == s1);
    }

    BOOST_AUTO_TEST_CASE(starts_with_)
    {
        BOOST_CHECK( starts_with(U"ABC"_sv, U"AB"_sv));
        BOOST_CHECK( starts_with(U"ABC"_sv, U"ABC"_sv));
        BOOST_CHECK(!starts_with(U"ABC"_sv, U"ABCD"_sv));
        BOOST_CHECK(!starts_with(U"ABC"_sv, U"BC"_sv));
    }

    BOOST_AUTO_TEST_CASE(ends_with_)
    {
        BOOST_CHECK( ends_with(U"ABC"_sv, U"BC"_sv));
        BOOST_CHECK( ends_with(U"ABC"_sv, U"ABC"_sv));
        BOOST_CHECK(!ends_with(U"ABC"_sv, U"DABC"_sv));
        BOOST_CHECK(!ends_with(U"ABC"_sv, U"AB"_sv));
    }

    BOOST_AUTO_TEST_CASE(repalce_)
    {
        string_t s;

        replace(s, [](auto c) { return c == U'A'; }, U'B');
        BOOST_CHECK(is_empty(s));

        s = U"ABC";
        replace(s, [](auto c) { return c == U'A'; }, U'B');
        BOOST_CHECK_EQUAL(s, U"BBC");

        replace(s, [](auto c) { return c == U'B'; }, U'C');
        BOOST_CHECK_EQUAL(s, U"CCC");
    }

    BOOST_AUTO_TEST_CASE(insert_byte)
    {
        string_t s;

        insert(s, 1, U'A');
        BOOST_CHECK(is_empty(s));

        insert(s, 0, U'1');
        BOOST_CHECK_EQUAL(s, U"1");

        insert(s, 0, U'2');
        BOOST_CHECK_EQUAL(s, U"21");

        insert(s, 2, U'3');
        BOOST_CHECK_EQUAL(s, U"213");

        insert(s, 1, U'4');
        BOOST_CHECK_EQUAL(s, U"2413");
    }

    BOOST_AUTO_TEST_CASE(insert_view)
    {
        string_t s;

        insert(s, 1, U"A");
        BOOST_CHECK(is_empty(s));

        insert(s, 0, U"11");
        BOOST_CHECK_EQUAL(s, U"11");

        insert(s, 0, U"22");
        BOOST_CHECK_EQUAL(s, U"2211");

        insert(s, 4, U"33");
        BOOST_CHECK_EQUAL(s, U"221133");

        insert(s, 1, U"44");
        BOOST_CHECK_EQUAL(s, U"24421133");
    }

    BOOST_AUTO_TEST_CASE(default_constructor)
    {
        string_t s;
        BOOST_CHECK(is_empty(s));
    }

    template<typename T>
    static void
    test_copy_constructor()
    {
        T const from = U"ABC";
        string_t s { from };

        BOOST_CHECK(s == from);
    }

    BOOST_AUTO_TEST_CASE(copy_constructor)
    {
        test_copy_constructor<string_t>();
        test_copy_constructor<string_view_t>();
        test_copy_constructor<std::u32string>();
        test_copy_constructor<std::u32string_view>();
        test_copy_constructor<char32_t const*>();
    }

    template<typename T>
    static void
    test_move_constructor()
    {
        T from = U"ABC";
        string_t s { std::move(from) };

        BOOST_CHECK(s == U"ABC");
    }

    BOOST_AUTO_TEST_CASE(move_constructor)
    {
        test_move_constructor<string_t>();
        test_move_constructor<std::u32string>();
    }

    template<typename T>
    static void
    test_copy_assignment()
    {
        T const from = U"ABC";
        string_t s;
        s = from;

        BOOST_CHECK(s == from);
    }

    BOOST_AUTO_TEST_CASE(copy_assignment)
    {
        test_copy_assignment<string_t>();
        test_copy_assignment<string_view_t>();
        test_copy_assignment<std::u32string>();
        test_copy_assignment<std::u32string_view>();
        test_copy_assignment<char32_t const*>();
    }

    template<typename T>
    static void
    test_move_assignment()
    {
        T from = U"ABC";
        string_t s;
        s = std::move(from);

        BOOST_CHECK(s == U"ABC");
    }

    BOOST_AUTO_TEST_CASE(move_assignment)
    {
        test_move_assignment<string_t>();
        test_move_assignment<std::u32string>();
    }

BOOST_AUTO_TEST_SUITE_END() // string_

BOOST_AUTO_TEST_SUITE(string_view_)

    BOOST_AUTO_TEST_CASE(default_constructor)
    {
        string_view_t s;
        BOOST_CHECK(is_empty(s));
    }

    template<typename T>
    static void
    test_copy_constructor()
    {
        T const from = U"ABC";
        string_view_t s { from };

        BOOST_CHECK(s == from);
    }

    BOOST_AUTO_TEST_CASE(copy_constructor)
    {
        test_copy_constructor<string_t>();
        test_copy_constructor<string_view_t>();
        test_copy_constructor<std::u32string>();
        test_copy_constructor<std::u32string_view>();
        test_copy_constructor<char32_t const*>();
    }

    template<typename T>
    static void
    test_copy_assignment()
    {
        T const from = U"ABC";
        string_view_t s;
        s = from;

        BOOST_CHECK(s == from);
    }

    BOOST_AUTO_TEST_CASE(copy_assignment)
    {
        test_copy_assignment<string_t>();
        test_copy_assignment<string_view_t>();
        test_copy_assignment<std::u32string>();
        test_copy_assignment<std::u32string_view>();
        test_copy_assignment<char32_t const*>();
    }

    template<typename T>
    static void
    test_construct_from_iterator()
    {
        T const from = U"ABC";
        string_view_t s { std::begin(from), std::end(from) };

        BOOST_CHECK(s == from);
    }

    BOOST_AUTO_TEST_CASE(construct_from_iterator)
    {
        test_construct_from_iterator<string_t>();
        test_construct_from_iterator<string_view_t>();
        test_copy_assignment<std::u32string>();
        test_copy_assignment<std::u32string_view>();
        test_copy_assignment<char32_t const*>();
    }

BOOST_AUTO_TEST_SUITE_END() // string_view_

} // namespace whatwg::infra::testing

