#include <whatwg/infra/list.hpp>

#include <whatwg/infra/ostream.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::infra {

BOOST_AUTO_TEST_SUITE(list_)

    BOOST_AUTO_TEST_CASE(append_)
    {
        list<int> l;
        BOOST_REQUIRE(is_empty(l));

        append(l, 100);
        BOOST_REQUIRE_EQUAL(size(l), 1);
        BOOST_CHECK_EQUAL(l[0], 100);

        append(l, 200);
        BOOST_REQUIRE_EQUAL(size(l), 2);
        BOOST_CHECK_EQUAL(l[0], 100);
        BOOST_CHECK_EQUAL(l[1], 200);
    }

    BOOST_AUTO_TEST_CASE(prepend_)
    {
        list<int> l;
        BOOST_REQUIRE(is_empty(l));

        prepend(l, 100);
        BOOST_REQUIRE_EQUAL(size(l), 1);
        BOOST_CHECK_EQUAL(l[0], 100);

        prepend(l, 200);
        BOOST_REQUIRE_EQUAL(size(l), 2);
        BOOST_CHECK_EQUAL(l[0], 200);
        BOOST_CHECK_EQUAL(l[1], 100);

        int i = 300;
        prepend(l, i);
        BOOST_REQUIRE_EQUAL(size(l), 3);
        BOOST_CHECK_EQUAL(l[0], 300);
        BOOST_CHECK_EQUAL(l[1], 200);
        BOOST_CHECK_EQUAL(l[2], 100);
    }

    BOOST_AUTO_TEST_CASE(replace_0)
    {
        list<int> l;
        auto is_one = [](auto i) { return i == 1; };

        replace(l, is_one, 5);

        BOOST_REQUIRE_EQUAL(size(l), 0);
    }

    BOOST_AUTO_TEST_CASE(replace_1)
    {
        list<int> l = { 0, 1, 2, 1, 3 };
        auto is_one = [](auto i) { return i == 1; };

        replace(l, is_one, 5);

        BOOST_REQUIRE_EQUAL(size(l), 5);
        BOOST_CHECK_EQUAL(l[0], 0);
        BOOST_CHECK_EQUAL(l[1], 5);
        BOOST_CHECK_EQUAL(l[2], 2);
        BOOST_CHECK_EQUAL(l[3], 5);
        BOOST_CHECK_EQUAL(l[4], 3);
    }

    BOOST_AUTO_TEST_CASE(replace_3)
    {
        list<int> l = { 0, 5, 2, 5, 3 };
        auto is_one = [](auto i) { return i == 1; };

        replace(l, is_one, 5);

        BOOST_REQUIRE_EQUAL(size(l), 5);
        BOOST_CHECK_EQUAL(l[0], 0);
        BOOST_CHECK_EQUAL(l[1], 5);
        BOOST_CHECK_EQUAL(l[2], 2);
        BOOST_CHECK_EQUAL(l[3], 5);
        BOOST_CHECK_EQUAL(l[4], 3);
    }

    BOOST_AUTO_TEST_CASE(insert_)
    {
        list<int> l;

        insert(l, 0, 100);
        BOOST_REQUIRE_EQUAL(size(l), 1);
        BOOST_CHECK_EQUAL(l[0], 100);

        insert(l, 0, 200);
        BOOST_REQUIRE_EQUAL(size(l), 2);
        BOOST_CHECK_EQUAL(l[0], 200);
        BOOST_CHECK_EQUAL(l[1], 100);

        insert(l, 2, 300);
        BOOST_REQUIRE_EQUAL(size(l), 3);
        BOOST_CHECK_EQUAL(l[0], 200);
        BOOST_CHECK_EQUAL(l[1], 100);
        BOOST_CHECK_EQUAL(l[2], 300);

        int const i = 400;
        insert(l, 2, i);
        BOOST_REQUIRE_EQUAL(size(l), 4);
        BOOST_CHECK_EQUAL(l[0], 200);
        BOOST_CHECK_EQUAL(l[1], 100);
        BOOST_CHECK_EQUAL(l[2], 400);
        BOOST_CHECK_EQUAL(l[3], 300);
    }

    BOOST_AUTO_TEST_CASE(remove_)
    {
        list<int> l = { 0, 1, 2, 3, 4, 5 };

        remove(l, [](auto i) { return i % 2 == 0; });

        BOOST_REQUIRE_EQUAL(size(l), 3);
        BOOST_CHECK_EQUAL(l[0], 1);
        BOOST_CHECK_EQUAL(l[1], 3);
        BOOST_CHECK_EQUAL(l[2], 5);
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        list<int> l = { 100, 200 };
        BOOST_REQUIRE_EQUAL(size(l), 2);

        empty(l);
        BOOST_REQUIRE_EQUAL(size(l), 0);
    }

    BOOST_AUTO_TEST_CASE(contains_)
    {
        list<int> const l = { 100, 200 };
        BOOST_CHECK(contains(l, 100));
        BOOST_CHECK(!contains(l, 300));
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        list<int> l;
        BOOST_CHECK_EQUAL(size(l), 0);

        append(l, 100);
        BOOST_CHECK_EQUAL(size(l), 1);
    }

    BOOST_AUTO_TEST_CASE(is_empty_)
    {
        list<int> l;
        BOOST_CHECK(is_empty(l));

        append(l, 100);
        BOOST_CHECK(!is_empty(l));
    }

BOOST_AUTO_TEST_SUITE_END() // list_

} // namespace whatwg::infra
