#include <whatwg/infra/queue.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::infra::testing {

BOOST_AUTO_TEST_SUITE(queue_)

    BOOST_AUTO_TEST_CASE(enqueue_)
    {
        queue<int> q;
        BOOST_CHECK(is_empty(q));

        enqueue(q, 100);
        BOOST_REQUIRE_EQUAL(size(q), 1);
        BOOST_CHECK_EQUAL(q[0], 100);

        enqueue(q, 200);
        BOOST_REQUIRE_EQUAL(size(q), 2);
        BOOST_CHECK_EQUAL(q[0], 100);
        BOOST_CHECK_EQUAL(q[1], 200);

        int const i = 300;
        enqueue(q, i);
        BOOST_REQUIRE_EQUAL(size(q), 3);
        BOOST_CHECK_EQUAL(q[0], 100);
        BOOST_CHECK_EQUAL(q[1], 200);
        BOOST_CHECK_EQUAL(q[2], 300);
    }

    BOOST_AUTO_TEST_CASE(dequeue_)
    {
        queue<int> q = { 100, 200 };
        BOOST_CHECK_EQUAL(size(q), 2);

        auto r = dequeue(q);
        BOOST_REQUIRE(r);
        BOOST_CHECK_EQUAL(*r, 100);

        r = dequeue(q);
        BOOST_REQUIRE(r);
        BOOST_CHECK_EQUAL(*r, 200);

        r = dequeue(q);
        BOOST_REQUIRE(!r);
    }

BOOST_AUTO_TEST_SUITE_END() // queue_

} // namespace whatwg::infra::testing
