#include <whatwg/infra/byte.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::infra::testing {

BOOST_AUTO_TEST_SUITE(byte_)

    BOOST_AUTO_TEST_CASE(is_ascii_byte_1)
    {
        BOOST_CHECK(is_ascii_byte('\x00'));
        BOOST_CHECK(is_ascii_byte('\x7F'));
        BOOST_CHECK(!is_ascii_byte('\x80'));
        BOOST_CHECK(!is_ascii_byte('\xFF'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_tab_or_newline_)
    {
        BOOST_CHECK(!is_ascii_tab_or_newline('\x00'));
        BOOST_CHECK(!is_ascii_tab_or_newline('\x08'));
        BOOST_CHECK( is_ascii_tab_or_newline('\x09'));
        BOOST_CHECK( is_ascii_tab_or_newline('\x0A'));
        BOOST_CHECK(!is_ascii_tab_or_newline('\x0B'));
        BOOST_CHECK(!is_ascii_tab_or_newline('\x0C'));
        BOOST_CHECK( is_ascii_tab_or_newline('\x0D'));
        BOOST_CHECK(!is_ascii_tab_or_newline('\x0E'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_whitespace_)
    {
        BOOST_CHECK(!is_ascii_whitespace('\x00'));
        BOOST_CHECK(!is_ascii_whitespace('\x08'));
        BOOST_CHECK( is_ascii_whitespace('\x09'));
        BOOST_CHECK( is_ascii_whitespace('\x0A'));
        BOOST_CHECK(!is_ascii_whitespace('\x0B'));
        BOOST_CHECK( is_ascii_whitespace('\x0C'));
        BOOST_CHECK( is_ascii_whitespace('\x0D'));
        BOOST_CHECK(!is_ascii_whitespace('\x0E'));
        BOOST_CHECK(!is_ascii_whitespace('\x1F'));
        BOOST_CHECK( is_ascii_whitespace('\x20'));
        BOOST_CHECK(!is_ascii_whitespace('\x21'));
    }

    BOOST_AUTO_TEST_CASE(is_c0_control_)
    {
        BOOST_CHECK( is_c0_control('\x00'));
        BOOST_CHECK( is_c0_control('\x1F'));
        BOOST_CHECK(!is_c0_control('\x20'));
    }

    BOOST_AUTO_TEST_CASE(is_c0_control_or_space_)
    {
        BOOST_CHECK( is_c0_control_or_space('\x00'));
        BOOST_CHECK( is_c0_control_or_space('\x1F'));
        BOOST_CHECK( is_c0_control_or_space('\x20'));
        BOOST_CHECK(!is_c0_control_or_space('\x21'));
    }

    BOOST_AUTO_TEST_CASE(is_control_)
    {
        BOOST_CHECK( is_control('\x00'));
        BOOST_CHECK( is_control('\x1F'));
        BOOST_CHECK(!is_control('\x20'));
        BOOST_CHECK(!is_control('\x7E'));
        BOOST_CHECK( is_control('\x7F'));
        BOOST_CHECK(!is_control('\x9F'));
        BOOST_CHECK(!is_control('\xA0'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_digit_)
    {
        BOOST_CHECK(!is_ascii_digit('\x00'));
        BOOST_CHECK(!is_ascii_digit('\x2F'));
        BOOST_CHECK( is_ascii_digit('\x30'));
        BOOST_CHECK( is_ascii_digit('\x39'));
        BOOST_CHECK(!is_ascii_digit('\x3A'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_upper_hex_digit_)
    {
        BOOST_CHECK(!is_ascii_upper_hex_digit('\x00'));
        BOOST_CHECK(!is_ascii_upper_hex_digit('\x2F'));
        BOOST_CHECK( is_ascii_upper_hex_digit('\x30'));
        BOOST_CHECK( is_ascii_upper_hex_digit('\x39'));
        BOOST_CHECK(!is_ascii_upper_hex_digit('\x3A'));
        BOOST_CHECK(!is_ascii_upper_hex_digit('\x40'));
        BOOST_CHECK( is_ascii_upper_hex_digit('\x41'));
        BOOST_CHECK( is_ascii_upper_hex_digit('\x46'));
        BOOST_CHECK(!is_ascii_upper_hex_digit('\x47'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_lower_hex_digit_)
    {
        BOOST_CHECK(!is_ascii_lower_hex_digit('\x00'));
        BOOST_CHECK(!is_ascii_lower_hex_digit('\x2F'));
        BOOST_CHECK( is_ascii_lower_hex_digit('\x30'));
        BOOST_CHECK( is_ascii_lower_hex_digit('\x39'));
        BOOST_CHECK(!is_ascii_lower_hex_digit('\x3A'));
        BOOST_CHECK(!is_ascii_lower_hex_digit('\x60'));
        BOOST_CHECK( is_ascii_lower_hex_digit('\x61'));
        BOOST_CHECK( is_ascii_lower_hex_digit('\x66'));
        BOOST_CHECK(!is_ascii_lower_hex_digit('\x67'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_hex_digit_)
    {
        BOOST_CHECK(!is_ascii_hex_digit('\x00'));
        BOOST_CHECK(!is_ascii_hex_digit('\x2F'));
        BOOST_CHECK( is_ascii_hex_digit('\x30'));
        BOOST_CHECK( is_ascii_hex_digit('\x39'));
        BOOST_CHECK(!is_ascii_hex_digit('\x3A'));
        BOOST_CHECK(!is_ascii_hex_digit('\x40'));
        BOOST_CHECK( is_ascii_hex_digit('\x41'));
        BOOST_CHECK( is_ascii_hex_digit('\x46'));
        BOOST_CHECK(!is_ascii_hex_digit('\x47'));
        BOOST_CHECK(!is_ascii_hex_digit('\x60'));
        BOOST_CHECK( is_ascii_hex_digit('\x61'));
        BOOST_CHECK( is_ascii_hex_digit('\x66'));
        BOOST_CHECK(!is_ascii_hex_digit('\x67'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_upper_alpha_)
    {
        BOOST_CHECK(!is_ascii_upper_alpha('\x00'));
        BOOST_CHECK(!is_ascii_upper_alpha('\x40'));
        BOOST_CHECK( is_ascii_upper_alpha('\x41'));
        BOOST_CHECK( is_ascii_upper_alpha('\x5A'));
        BOOST_CHECK(!is_ascii_upper_alpha('\x5B'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_lower_alpha_)
    {
        BOOST_CHECK(!is_ascii_lower_alpha('\x00'));
        BOOST_CHECK(!is_ascii_lower_alpha('\x60'));
        BOOST_CHECK( is_ascii_lower_alpha('\x61'));
        BOOST_CHECK( is_ascii_lower_alpha('\x7A'));
        BOOST_CHECK(!is_ascii_lower_alpha('\x7B'));
    }

    BOOST_AUTO_TEST_CASE(to_ascii_upper_)
    {
        BOOST_CHECK_EQUAL(to_ascii_upper('\x00'), '\x00');
        BOOST_CHECK_EQUAL(to_ascii_upper('\x40'), '\x40');
        BOOST_CHECK_EQUAL(to_ascii_upper('\x41'), '\x41');
        BOOST_CHECK_EQUAL(to_ascii_upper('\x5A'), '\x5A');
        BOOST_CHECK_EQUAL(to_ascii_upper('\x5B'), '\x5B');
        BOOST_CHECK_EQUAL(to_ascii_upper('\x60'), '\x60');
        BOOST_CHECK_EQUAL(to_ascii_upper('\x61'), '\x41');
        BOOST_CHECK_EQUAL(to_ascii_upper('\x7A'), '\x5A');
        BOOST_CHECK_EQUAL(to_ascii_upper('\x7B'), '\x7B');
    }

    BOOST_AUTO_TEST_CASE(to_ascii_lower_)
    {
        BOOST_CHECK_EQUAL(to_ascii_lower('\x00'), '\x00');
        BOOST_CHECK_EQUAL(to_ascii_lower('\x40'), '\x40');
        BOOST_CHECK_EQUAL(to_ascii_lower('\x41'), '\x61');
        BOOST_CHECK_EQUAL(to_ascii_lower('\x5A'), '\x7A');
        BOOST_CHECK_EQUAL(to_ascii_lower('\x5B'), '\x5B');
        BOOST_CHECK_EQUAL(to_ascii_lower('\x60'), '\x60');
        BOOST_CHECK_EQUAL(to_ascii_lower('\x61'), '\x61');
        BOOST_CHECK_EQUAL(to_ascii_lower('\x7A'), '\x7A');
        BOOST_CHECK_EQUAL(to_ascii_lower('\x7B'), '\x7B');
    }

    BOOST_AUTO_TEST_CASE(is_ascii_alpha_)
    {
        BOOST_CHECK(!is_ascii_alpha('\x00'));
        BOOST_CHECK(!is_ascii_alpha('\x40'));
        BOOST_CHECK( is_ascii_alpha('\x41'));
        BOOST_CHECK( is_ascii_alpha('\x5A'));
        BOOST_CHECK(!is_ascii_alpha('\x5B'));
        BOOST_CHECK(!is_ascii_alpha('\x60'));
        BOOST_CHECK( is_ascii_alpha('\x61'));
        BOOST_CHECK( is_ascii_alpha('\x7A'));
        BOOST_CHECK(!is_ascii_alpha('\x7B'));
    }

    BOOST_AUTO_TEST_CASE(is_ascii_alphanumeric_)
    {
        BOOST_CHECK(!is_ascii_alphanumeric('\x00'));
        BOOST_CHECK(!is_ascii_alphanumeric('\x2F'));
        BOOST_CHECK( is_ascii_alphanumeric('\x30'));
        BOOST_CHECK( is_ascii_alphanumeric('\x39'));
        BOOST_CHECK(!is_ascii_alphanumeric('\x3A'));
        BOOST_CHECK(!is_ascii_alphanumeric('\x40'));
        BOOST_CHECK( is_ascii_alphanumeric('\x41'));
        BOOST_CHECK( is_ascii_alphanumeric('\x5A'));
        BOOST_CHECK(!is_ascii_alphanumeric('\x5B'));
        BOOST_CHECK(!is_ascii_alphanumeric('\x60'));
        BOOST_CHECK( is_ascii_alphanumeric('\x61'));
        BOOST_CHECK( is_ascii_alphanumeric('\x7A'));
        BOOST_CHECK(!is_ascii_alphanumeric('\x7B'));
    }

BOOST_AUTO_TEST_SUITE_END() // byte_

} // namespace whatwg::infra::testing
