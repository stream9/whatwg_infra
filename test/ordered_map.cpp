#include <whatwg/infra/ordered_map.hpp>

#include <whatwg/infra/ordered_set.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::infra::testing {

BOOST_AUTO_TEST_SUITE(ordered_map_)

    BOOST_AUTO_TEST_CASE(construct_)
    {
        ordered_map<int, int> m1;
        ordered_map<int, int> m2 = { { 1, 100 }, { 2, 200 } };

        auto const m3 { m2 };
        auto const m4 { std::move(m2) };

        auto const m5 = m3;
        auto const m6 = std::move(m3);
    }

    BOOST_AUTO_TEST_CASE(subscript_1)
    {
        ordered_map<int, int> const m1 = { { 1, 100 }, { 2, 200 } };

        BOOST_CHECK_EQUAL(m1[2], 200);
        BOOST_CHECK_THROW(m1[3], std::out_of_range);
    }

    BOOST_AUTO_TEST_CASE(subscript_2)
    {
        ordered_map<int, int> m1;

        m1[3] = 100;
        BOOST_REQUIRE_EQUAL(size(m1), 1);
        BOOST_CHECK_EQUAL(m1[3], 100);

        m1[5] = 200;
        BOOST_REQUIRE_EQUAL(size(m1), 2);
        BOOST_CHECK_EQUAL(m1[3], 100);
        BOOST_CHECK_EQUAL(m1[5], 200);

        m1[3] = 300;
        BOOST_REQUIRE_EQUAL(size(m1), 2);
        BOOST_CHECK_EQUAL(m1[3], 300);
        BOOST_CHECK_EQUAL(m1[5], 200);
    }

    BOOST_AUTO_TEST_CASE(iterator_)
    {
        ordered_map<int, int> m1;
        BOOST_CHECK(m1.begin() == m1.end());

        m1[1] = 100;
        BOOST_CHECK(m1.begin() != m1.end());
        BOOST_CHECK(m1.begin() + 1 == m1.end());
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        ordered_map<int, int> m1;
        BOOST_CHECK_EQUAL(m1.size(), 0);

        m1[1] = 100;
        BOOST_CHECK_EQUAL(m1.size(), 1);
        BOOST_CHECK_EQUAL(size(m1), 1);

        m1[5] = 100;
        BOOST_CHECK_EQUAL(m1.size(), 2);
        BOOST_CHECK_EQUAL(size(m1), 2);
    }

    BOOST_AUTO_TEST_CASE(find_)
    {
        ordered_map<int, int> const m1 = { { 1, 100 }, { 5, 500 } };

        BOOST_REQUIRE(m1.find(1) != m1.end());
        BOOST_CHECK_EQUAL(m1.find(1)->value, 100);

        BOOST_CHECK(m1.find(3) == m1.end());

        BOOST_REQUIRE(m1.find(5) != m1.end());
        BOOST_CHECK_EQUAL(m1.find(5)->value, 500);
    }

    BOOST_AUTO_TEST_CASE(contains_)
    {
        ordered_map<int, int> const m1 = { { 1, 100 }, { 5, 500 } };

        BOOST_CHECK( m1.contains(1));
        BOOST_CHECK(!m1.contains(3));
        BOOST_CHECK( m1.contains(5));
    }

    BOOST_AUTO_TEST_CASE(erase_)
    {
        ordered_map<int, int> m1 = { { 1, 100 }, { 5, 500 } };

        BOOST_REQUIRE_EQUAL(m1.size(), 2);
        m1.erase(m1.begin());
        BOOST_REQUIRE_EQUAL(m1.size(), 1);
        BOOST_REQUIRE(m1.find(1) == m1.end());
    }

    BOOST_AUTO_TEST_CASE(get_the_value_of_an_entry_)
    {
        ordered_map<int, int> const m1 = { { 1, 100 }, { 5, 500 } };

        BOOST_REQUIRE(m1[1]);
        BOOST_REQUIRE(get_the_value_of_an_entry(m1, 1));
        BOOST_CHECK_EQUAL(*get_the_value_of_an_entry(m1, 1), 100);

        BOOST_REQUIRE(!get_the_value_of_an_entry(m1, 3));
    }

    BOOST_AUTO_TEST_CASE(set_the_value_of_an_entry_)
    {
        ordered_map<int, int> m1;
        BOOST_REQUIRE_EQUAL(m1.size(), 0);

        set_the_value_of_an_entry(m1, 1, 200);
        BOOST_REQUIRE_EQUAL(m1.size(), 1);
        BOOST_REQUIRE(m1.find(1) != m1.end());
        BOOST_CHECK_EQUAL(m1[1], 200);
    }

    BOOST_AUTO_TEST_CASE(remove_an_entry_)
    {
        ordered_map<int, int> m1 = { { 1, 100 }, { 5, 500 } };
        auto has_odd_value = [](auto const& e) { return e.value % 2 == 1; };
        auto has_even_value = [](auto const& e) { return e.value % 2 == 0; };

        BOOST_REQUIRE_EQUAL(m1.size(), 2);

        remove_an_entry(m1, has_odd_value);
        BOOST_REQUIRE_EQUAL(m1.size(), 2);

        remove_an_entry(m1, has_even_value);
        BOOST_REQUIRE_EQUAL(m1.size(), 0);

        m1 = { { 1, 1 }, { 2, 2 } };
        remove_an_entry(m1, has_odd_value);
        BOOST_REQUIRE_EQUAL(m1.size(), 1);
    }

    BOOST_AUTO_TEST_CASE(contains_an_entry_with_a_given_key_)
    {
        ordered_map<int, int> const m1 = { { 1, 100 }, { 5, 500 } };

        BOOST_CHECK( contains_an_entry_with_a_given_key(m1, 1));
        BOOST_CHECK(!contains_an_entry_with_a_given_key(m1, 3));
        BOOST_CHECK( contains_an_entry_with_a_given_key(m1, 5));
    }

    BOOST_AUTO_TEST_CASE(get_the_keys_)
    {
        ordered_map<int, int> m1;

        auto r = get_the_keys(m1);
        BOOST_REQUIRE(is_empty(r));

        m1 = { { 1, 10 }, { 2, 20 } };
        r = get_the_keys(m1);
        BOOST_REQUIRE_EQUAL(size(r), 2);
        BOOST_CHECK_EQUAL(r[0], 1);
        BOOST_CHECK_EQUAL(r[1], 2);
    }

    BOOST_AUTO_TEST_CASE(get_the_values_)
    {
        ordered_map<int, int> m1;

        auto r = get_the_values(m1);
        BOOST_REQUIRE(is_empty(r));

        m1 = { { 1, 10 }, { 2, 20 } };
        r = get_the_values(m1);
        BOOST_REQUIRE_EQUAL(size(r), 2);
        BOOST_CHECK_EQUAL(r[0], 10);
        BOOST_CHECK_EQUAL(r[1], 20);
    }

    BOOST_AUTO_TEST_CASE(is_empty_)
    {
        ordered_map<int, int> m1;
        BOOST_REQUIRE(is_empty(m1));

        m1[5] = 20;
        BOOST_REQUIRE(!is_empty(m1));
    }

BOOST_AUTO_TEST_SUITE_END() // ordered_map_

} // namespace whatwg::infra::testing
