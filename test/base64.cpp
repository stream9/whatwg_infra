#include <whatwg/infra/base64.hpp>

#include <whatwg/infra/byte_sequence.hpp>
#include <whatwg/infra/ostream.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::infra::testing {

BOOST_AUTO_TEST_SUITE(base64_)

    static void
    test_encode(byte_sequence_view_t const input, string_view_t const expected)
    {
        auto const r = forgiving_base64_encode(input);
        BOOST_TEST_INFO("input: " << input);
        BOOST_CHECK_EQUAL(r, expected);
    }

    BOOST_AUTO_TEST_CASE(encode_)
    {
        test_encode("",  U"");
        test_encode("TEST",  U"VEVTVA==");
        test_encode("TEST2", U"VEVTVDI=");
        test_encode("123456789012", U"MTIzNDU2Nzg5MDEy");
        test_encode("\xFC", U"/A==");
        test_encode("\xF8", U"+A==");
    }

    BOOST_AUTO_TEST_CASE(encode_rfc4648_test_vector)
    {
        test_encode("", U"");
        test_encode("f", U"Zg==");
        test_encode("fo", U"Zm8=");
        test_encode("foo", U"Zm9v");
        test_encode("foob", U"Zm9vYg==");
        test_encode("fooba", U"Zm9vYmE=");
        test_encode("foobar", U"Zm9vYmFy");
    }

    static void
    test_decode(string_view_t const input, byte_sequence_view_t const expected)
    {
        auto const r = forgiving_base64_decode(input);
        BOOST_TEST_INFO("input: " << input);
        BOOST_REQUIRE(r);
        BOOST_CHECK_EQUAL(*r, expected);
    }

    BOOST_AUTO_TEST_CASE(decode_success)
    {
        test_decode(U"", "");
        test_decode(U"VEVTVA==", "TEST");
        test_decode(U"VEVTVDI=", "TEST2");
        test_decode(U"MTIzNDU2Nzg5MDEy", "123456789012");
        test_decode(U"/A==", "\xFC");
        test_decode(U"+A==", "\xF8");
    }

    static void
    decode_failure(string_view_t const input)
    {
        auto const r = forgiving_base64_decode(input);
        BOOST_TEST_INFO("input: " << input);
        BOOST_REQUIRE(!r);
    }

    BOOST_AUTO_TEST_CASE(decode_failure_)
    {
        decode_failure(U"ABC*&%$123");
        decode_failure(U"A");
    }

    BOOST_AUTO_TEST_CASE(decode_rfc4648_test_vector)
    {
        test_decode(U"", "");
        test_decode(U"Zg==", "f");
        test_decode(U"Zm8=", "fo");
        test_decode(U"Zm9v", "foo");
        test_decode(U"Zm9vYg==", "foob");
        test_decode(U"Zm9vYmE=", "fooba");
        test_decode(U"Zm9vYmFy", "foobar");
    }

BOOST_AUTO_TEST_SUITE_END() // base64_

} // namespace whatwg::infra::testing
