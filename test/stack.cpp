#include <whatwg/infra/stack.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::infra::testing {

BOOST_AUTO_TEST_SUITE(stack_)

    BOOST_AUTO_TEST_CASE(push_)
    {
        stack<int> s;
        BOOST_CHECK(is_empty(s));

        push(s, 100);
        BOOST_REQUIRE_EQUAL(size(s), 1);
        BOOST_CHECK_EQUAL(s[0], 100);

        push(s, 200);
        BOOST_REQUIRE_EQUAL(size(s), 2);
        BOOST_CHECK_EQUAL(s[0], 100);
        BOOST_CHECK_EQUAL(s[1], 200);

        int const i = 300;
        push(s, i);
        BOOST_REQUIRE_EQUAL(size(s), 3);
        BOOST_CHECK_EQUAL(s[0], 100);
        BOOST_CHECK_EQUAL(s[1], 200);
        BOOST_CHECK_EQUAL(s[2], 300);
    }

    BOOST_AUTO_TEST_CASE(pop_)
    {
        stack<int> s = { 100, 200 };
        BOOST_CHECK_EQUAL(size(s), 2);

        auto r = pop(s);
        BOOST_REQUIRE(r);
        BOOST_CHECK_EQUAL(*r, 200);

        r = pop(s);
        BOOST_REQUIRE(r);
        BOOST_CHECK_EQUAL(*r, 100);

        r = pop(s);
        BOOST_REQUIRE(!r);
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        stack<int> s = { 100, 200 };
        BOOST_REQUIRE_EQUAL(size(s), 2);

        empty(s);
        BOOST_REQUIRE_EQUAL(size(s), 0);
    }

    BOOST_AUTO_TEST_CASE(contains_)
    {
        stack<int> const s = { 100, 200 };
        BOOST_CHECK(contains(s, 100));
        BOOST_CHECK(!contains(s, 300));
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        stack<int> s;
        BOOST_CHECK_EQUAL(size(s), 0);

        push(s, 100);
        BOOST_CHECK_EQUAL(size(s), 1);
    }

    BOOST_AUTO_TEST_CASE(is_empty_)
    {
        stack<int> s;
        BOOST_CHECK(is_empty(s));

        push(s, 100);
        BOOST_CHECK(!is_empty(s));
    }

BOOST_AUTO_TEST_SUITE_END() // stack_

} // namespace whatwg::infra::testing
