#include <whatwg/infra/ordered_set.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::infra::testing {

BOOST_AUTO_TEST_SUITE(ordered_set_)

    BOOST_AUTO_TEST_CASE(append_)
    {
        ordered_set<int> s;
        BOOST_CHECK(is_empty(s));

        append(s, 300);
        append(s, 200);
        append(s, 100);
        append(s, 300);
        BOOST_CHECK_EQUAL(size(s), 3);

        int i = 200;
        append(s, i);
        BOOST_CHECK_EQUAL(size(s), 3);

        i = 400;
        append(s, i);
        BOOST_CHECK_EQUAL(size(s), 4);

        BOOST_CHECK_EQUAL(s[0], 300);
        BOOST_CHECK_EQUAL(s[1], 200);
        BOOST_CHECK_EQUAL(s[2], 100);
        BOOST_CHECK_EQUAL(s[3], 400);
    }

    BOOST_AUTO_TEST_CASE(prepend_)
    {
        ordered_set<int> s;

        prepend(s, 300);
        prepend(s, 200);
        prepend(s, 100);
        prepend(s, 300);
        BOOST_CHECK_EQUAL(size(s), 3);

        int i = 200;
        prepend(s, i);
        BOOST_CHECK_EQUAL(size(s), 3);

        i = 400;
        prepend(s, i);
        BOOST_CHECK_EQUAL(size(s), 4);

        BOOST_CHECK_EQUAL(s[0], 400);
        BOOST_CHECK_EQUAL(s[1], 100);
        BOOST_CHECK_EQUAL(s[2], 200);
        BOOST_CHECK_EQUAL(s[3], 300);
    }

    BOOST_AUTO_TEST_CASE(replace_1)
    {
        ordered_set<int> s = { 1, 2, 3 };
        replace(s, 1, 3);

        BOOST_CHECK_EQUAL(size(s), 2);
        BOOST_CHECK_EQUAL(s[0], 3);
        BOOST_CHECK_EQUAL(s[1], 2);
    }

    BOOST_AUTO_TEST_CASE(replace_2)
    {
        ordered_set<int> s = { 3, 2, 1 };
        replace(s, 1, 3);

        BOOST_CHECK_EQUAL(size(s), 2);
        BOOST_CHECK_EQUAL(s[0], 3);
        BOOST_CHECK_EQUAL(s[1], 2);
    }

    BOOST_AUTO_TEST_CASE(is_subset_0)
    {
        ordered_set<int> const s1 = { 1, 2, 3, 4, 5 };
        ordered_set<int> const s2;

        BOOST_CHECK( is_subset(s2, s1));
        BOOST_CHECK(!is_subset(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(is_subset_1)
    {
        ordered_set<int> const s1 = { 1, 2, 3, 4, 5 };
        ordered_set<int> const s2 = { 3, 1 };

        BOOST_CHECK( is_subset(s2, s1));
        BOOST_CHECK(!is_subset(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(is_superset_0)
    {
        ordered_set<int> const s1 = { 1, 2, 3, 4, 5 };
        ordered_set<int> const s2;

        BOOST_CHECK(!is_superset(s2, s1));
        BOOST_CHECK( is_superset(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(is_superset_1)
    {
        ordered_set<int> const s1 = { 1, 2, 3, 4, 5 };
        ordered_set<int> const s2 = { 3, 1 };

        BOOST_CHECK(!is_superset(s2, s1));
        BOOST_CHECK( is_superset(s1, s2));
    }

    BOOST_AUTO_TEST_CASE(intersection_of_0)
    {
        ordered_set<int> const s1 = { 1, 2, 3, 4, 5 };
        ordered_set<int> const s2;

        auto const r = intersection_of(s1, s2);
        BOOST_CHECK(is_empty(r));
    }

    BOOST_AUTO_TEST_CASE(intersection_of_1)
    {
        ordered_set<int> const s1 = { 1, 2, 3, 4, 5 };
        ordered_set<int> const s2 = { 3, 1, 7, 9, 10 };

        auto const r = intersection_of(s1, s2);
        BOOST_REQUIRE_EQUAL(size(r), 2);
        BOOST_CHECK_EQUAL(r[0], 1);
        BOOST_CHECK_EQUAL(r[1], 3);
    }

    BOOST_AUTO_TEST_CASE(union_of_0)
    {
        ordered_set<int> const s1 = { 1, 2, 3, 4, 5 };
        ordered_set<int> const s2;

        auto const r = union_of(s1, s2);
        BOOST_CHECK(r == s1);
    }

    BOOST_AUTO_TEST_CASE(union_of_1)
    {
        ordered_set<int> const s1 = { 1, 2, 3, 4, 5 };
        ordered_set<int> const s2 = { 3, 4, 5, 6, 7 };

        auto const r = union_of(s1, s2);
        BOOST_REQUIRE_EQUAL(size(r), 7);
        BOOST_CHECK_EQUAL(r[0], 1);
        BOOST_CHECK_EQUAL(r[1], 2);
        BOOST_CHECK_EQUAL(r[2], 3);
        BOOST_CHECK_EQUAL(r[3], 4);
        BOOST_CHECK_EQUAL(r[4], 5);
        BOOST_CHECK_EQUAL(r[5], 6);
        BOOST_CHECK_EQUAL(r[6], 7);
    }

    BOOST_AUTO_TEST_CASE(the_range_)
    {
        auto const r = the_range(1, 4);

        BOOST_REQUIRE_EQUAL(size(r), 4);
        BOOST_CHECK_EQUAL(r[0], 1);
        BOOST_CHECK_EQUAL(r[1], 2);
        BOOST_CHECK_EQUAL(r[2], 3);
        BOOST_CHECK_EQUAL(r[3], 4);
    }

    BOOST_AUTO_TEST_CASE(empty_)
    {
        ordered_set<int> s = { 100, 200 };
        BOOST_REQUIRE_EQUAL(size(s), 2);

        empty(s);
        BOOST_REQUIRE_EQUAL(size(s), 0);
    }

    BOOST_AUTO_TEST_CASE(contains_)
    {
        ordered_set<int> const s = { 100, 200 };
        BOOST_CHECK(contains(s, 100));
        BOOST_CHECK(!contains(s, 300));
    }

    BOOST_AUTO_TEST_CASE(size_)
    {
        ordered_set<int> s;
        BOOST_CHECK_EQUAL(size(s), 0);

        append(s, 100);
        BOOST_CHECK_EQUAL(size(s), 1);
    }

    BOOST_AUTO_TEST_CASE(is_empty_)
    {
        ordered_set<int> s;
        BOOST_CHECK(is_empty(s));

        append(s, 100);
        BOOST_CHECK(!is_empty(s));
    }

BOOST_AUTO_TEST_SUITE_END() // ordered_set_

} // namespace whatwg::infra::testing
