#include <whatwg/infra/ostream.hpp>

#include <whatwg/infra/string.hpp>

#include <sstream>

#include <boost/test/unit_test.hpp>

namespace whatwg::infra::testing {

BOOST_AUTO_TEST_SUITE(ostream_)

    BOOST_AUTO_TEST_CASE(output_string_view_t_)
    {
        std::ostringstream oss;
        oss << U"TEST"_sv;

        BOOST_CHECK_EQUAL(oss.str(), "TEST");
    }

    BOOST_AUTO_TEST_CASE(output_string_t_)
    {
        std::ostringstream oss;
        oss << U"TEST"_s;

        BOOST_CHECK_EQUAL(oss.str(), "TEST");
    }

    BOOST_AUTO_TEST_CASE(output_code_point_pointer_)
    {
        std::ostringstream oss;
        oss << U"A";

        BOOST_CHECK_EQUAL(oss.str(), "A");
    }

    BOOST_AUTO_TEST_CASE(output_code_point_)
    {
        std::ostringstream oss;
        oss << U'A';

        BOOST_CHECK_EQUAL(oss.str(), "A");
    }

BOOST_AUTO_TEST_SUITE_END() // ostream_

} // namespace whatwg::infra
