#include <whatwg/infra/primitive.hpp>

#include <boost/test/unit_test.hpp>

namespace whatwg::infra::testing {

BOOST_AUTO_TEST_SUITE(primitive_)

    BOOST_AUTO_TEST_CASE(null_)
    {
        nullable<int> a;
        BOOST_CHECK(a == null);

        a = 1;
        BOOST_CHECK(a != null);
    }

BOOST_AUTO_TEST_SUITE_END() // primitive_

} // namespace whatwg::infra::testing
