Introduction
------------
This is an C++ implementation of [WHATWG Infra Standard](https://infra.spec.whatwg.org/).  
It is compliant with specification [last updated 13 September 2018](https://infra.spec.whatwg.org/commit-snapshots/dbb1e179fb71ae11e2e5c3f16e18e0ba28ac8718/).

Requrement
----------
- C++17
- [Boost library](https://www.boost.org/)
- [CMake](https://cmake.org) version 3.3+
- [Doxygen](http://doxygen.nl/) (if you want generate documentation)

Getting Started
---------------
1. Download source code manually or clone repository as git submodule.
2. Import with add_subdirectory() from parent project's CMakeLists.txt.
3. Add "whatwg_infra" target to your project with target_link_libraries().

[API Document](https://stream9.gitlab.io/whatwg_infra/modules.html)
------------

Note
----
- JSON library is not included
- JavaScript string, scalar string are'nt included, only code_point string and byte sequence are provided.
- Specification assume containers adopt reference semantics but they are implemented
  with value semantics in this implementation, so if you want create shallow copy of 
  items, use std::shared_ptr etc as items.
