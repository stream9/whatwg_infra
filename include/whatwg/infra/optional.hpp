//! \file
#ifndef WHATWG_INFRA_OPTIONAL_HPP
#define WHATWG_INFRA_OPTIONAL_HPP

#include <optional>

namespace whatwg::infra {

template<typename T>
using optional = std::optional<T>;

} // namespace whatwg::infra

#endif // WHATWG_INFRA_OPTIONAL_HPP
