#ifndef WHATWG_INFRA_OSTREAM_HPP
#define WHATWG_INFRA_OSTREAM_HPP

#include "code_point_fwd.hpp"
#include "string_fwd.hpp"

#include "detail/code_convert.hpp"

#include <ostream>
#include <string_view>

namespace whatwg::infra {

inline std::ostream&
operator<<(std::ostream& os, whatwg::infra::string_view_t const s)
{
    return os << whatwg::infra::detail::to_utf8(s);
}

inline std::ostream&
operator<<(std::ostream& os, whatwg::infra::code_point_t const* const s)
{
    return os << whatwg::infra::detail::to_utf8(s);
}

inline std::ostream&
operator<<(std::ostream& os, whatwg::infra::code_point_t const c)
{
    code_point_t const buf[] = { c, U'\0' };
    return os << whatwg::infra::detail::to_utf8(buf);
}

} // namespace whatwg::infra

#endif // WHATWG_INFRA_OSTREAM_HPP
