//! \file
//! [4.6. Strings](https://infra.spec.whatwg.org/#strings)
#ifndef WHATWG_INFRA_STRING_HPP
#define WHATWG_INFRA_STRING_HPP

#include "algorithm.hpp"
#include "byte_sequence.hpp"
#include "code_point.hpp"
#include "list.hpp"
#include "string_fwd.hpp"

#include "detail/string.hpp"

#include <algorithm>

#include <boost/algorithm/string/predicate.hpp>

namespace whatwg::infra {

//! \addtogroup string String
//! [4.6. Strings](https://infra.spec.whatwg.org/#strings)
//!@{

//TODO javascript_string, scalar_value_string

inline void
append(string_t& s, code_point_t const c)
{
    s.push_back(c);
}

inline void
append(string_t& s1, string_view_t const s2)
{
    s1.append(s2);
}

inline void
prepend(string_t& s, code_point_t const c)
{
    s.insert(0, 1, c);
}

inline void
prepend(string_t& s1, string_view_t const s2)
{
    s1.insert(0, s2);
}

//! \brief Replace all code points that match a given condition
//!        with the given code point, or do nothing if none match the
//!        condition.
template<typename Predicate>
void
replace(string_t& s, Predicate condition, code_point_t const c)
{
    std::replace_if(s.begin(), s.end(), condition, c);
}

//! \detail Do nothing if index is out of range.
inline void
insert(string_t& s, position_t const index, code_point_t const c)
{
    if (index > size(s)) return;

    s.insert(index, 1, c);
}

//! \param s1 target
//! \param s2 byte sequence to insert
//! \detail Do nothing if index is out of range.
inline void
insert(string_t& s1, position_t const index, string_view_t const s2)
{
    if (index > size(s1)) return;

    s1.insert(index, s2);
}

//! \brief Remove length code points of substring from position pos.
inline void
remove(string_t& s, position_t const pos, size_t const length)
{
    s.erase(pos, length);
}

//! \brief Remove all items from the string that match a given condition,
//!        or do nothing if none do.
template<typename Predicate>
void
remove(string_t& s, Predicate condition)
{
    s.erase(
        std::remove_if(s.begin(), s.end(), condition),
        s.end()
    );
}

inline size_t
length(string_view_t const string)
{
    return string.size();
}

inline bool
is_empty(string_view_t const s)
{
    return s.empty();
}

inline bool
is_ascii_string(string_view_t const string)
{
    return std::all_of(
        string.begin(), string.end(),
        [](auto const c) {
            return is_ascii_code_point(c);
        });
}

inline bool
ascii_case_insensitive_match(
    string_view_t const a, string_view_t const b)
{
    auto is_ascii_iequal = [](auto a, auto b) {
        return to_ascii_lower(a) == to_ascii_lower(b);
    };

    return boost::equals(a, b, is_ascii_iequal);
}

inline bool
starts_with(string_view_t const s1, string_view_t const s2)
{
    namespace ba = boost::algorithm;
    return ba::starts_with(s1, s2);
}

inline bool
starts_with(string_view_t const s, code_point_t c)
{
    return !is_empty(s) && s[0] == c;
}

inline bool
ends_with(string_view_t const s1, string_view_t const s2)
{
    namespace ba = boost::algorithm;
    return ba::ends_with(s1, s2);
}

inline bool
ends_with(string_view_t const s, code_point_t c)
{
    return !is_empty(s) && s[length(s) - 1] == c;
}

template<typename Predicate>
bool
contains(string_view_t const s, Predicate condition)
{
    return std::any_of(s.begin(), s.end(), condition);
}

inline byte_sequence_t
isomorphic_encode(string_view_t const input)
{
    byte_sequence_t result;

    result.reserve(length(input));

    for (auto const c: input) {
        assert(c <= U'\xFF');
        result.push_back(to_byte(c));
    }

    return result;
}

inline byte_sequence_t
ascii_encode(string_view_t const input)
{
    assert(is_ascii_string(input));

    return isomorphic_encode(input);
}

inline string_t
ascii_decode(byte_sequence_view_t const input)
{
    assert(std::all_of(input.begin(), input.end(), is_ascii_byte));

    return isomorphic_decode(input);
}

inline void
ascii_lowercase(string_t& s)
{
    for (auto& c: s) {
        c = to_ascii_lower(c);
    }
}

inline string_t
ascii_lowercase_copy(string_view_t const s)
{
    string_t result;

    result.reserve(length(s));

    for (auto const c: s) {
        result.push_back(to_ascii_lower(c));
    }

    return result;
}

inline void
ascii_uppercase(string_t& s)
{
    for (auto& c: s) {
        c = to_ascii_upper(c);
    }
}

inline string_t
ascii_uppercase_copy(string_view_t const s)
{
    string_t result;

    result.reserve(length(s));

    for (auto const c: s) {
        result.push_back(to_ascii_upper(c));
    }

    return result;
}

inline void
strip_newlines(string_t& s)
{
    auto it = s.begin();
    auto const end = s.end();

    for (; it != end; ++it) {
        while (*it == U'\x0A' || *it == U'\x0D') {
            s.erase(it);
        }
    }
}

inline void
strip_leading_and_trailing_ascii_whitespace(string_t& s)
{
    auto erase = [](auto& s, auto const idx, auto const n) {
        s.erase(idx, n);
    };

    auto pred = static_cast<bool(*)(code_point_t)>(is_ascii_whitespace);

    detail::ltrim(s, pred, erase);
    detail::rtrim(s, pred, erase);
}

inline void
strip_leading_and_trailing_ascii_whitespace(string_view_t& s)
{
    auto lerase = [](auto& s, auto, auto const n) {
        s.remove_prefix(n);
    };

    auto rerase = [](auto& s, auto, auto const n) {
        s.remove_suffix(n);
    };

    auto pred = static_cast<bool(*)(code_point_t)>(is_ascii_whitespace);

    detail::ltrim(s, pred, lerase);
    detail::rtrim(s, pred, rerase);
}

inline void
strip_and_collapse_ascii_whitespace(string_t& s)
{
    bool ws = false;
    size_t ws_from = 0;

    strip_leading_and_trailing_ascii_whitespace(s);

    for (size_t i = 0; i < length(s); ++i) {
        if (is_ascii_whitespace(s[i])) {
            if (!ws) {
                ws = true;
                ws_from = i;
            }
        }
        else {
            if (ws) {
                auto const count = i - ws_from;
                s.replace(ws_from, count, U"\x20");

                ws = false;
            }
        }
    }
}

template<typename Predicate>
string_view_t
collect_a_sequence_of_code_point(
    string_view_t const input, position_t& position, Predicate condition)
{
    auto const start = position;
    auto const past_the_end = length(input);

    while (position < past_the_end && condition(input[position])) {
        ++position;
    }

    return { input.data() + start, position - start };
}

inline void
skip_ascii_whitespace(string_view_t const input, position_t& position)
{
    auto pred = static_cast<bool(*)(code_point_t)>(is_ascii_whitespace);

    collect_a_sequence_of_code_point(
                            input, position, pred);
}

inline list<string_view_t>
strictly_split(string_view_t const input, code_point_t const delimiter)
{
    position_t position = 0;
    list<string_view_t> tokens;

    auto is_not_delimiter =
        [delimiter](auto const c) {
            return c != delimiter;
        };

    auto const token = collect_a_sequence_of_code_point(
        input, position, is_not_delimiter
    );

    append(tokens, token);

    auto const past_the_end = length(input);
    while (position < past_the_end) {
        ++position;

        auto const token = collect_a_sequence_of_code_point(
            input, position, is_not_delimiter
        );

        append(tokens, token);
    }

    return tokens;
}

inline list<string_view_t>
split_on_ascii_whitespace(string_view_t const input)
{
    position_t position = 0;
    list<string_view_t> tokens;

    skip_ascii_whitespace(input, position);

    auto const past_the_end = length(input);
    while (position < past_the_end) {
        auto const token = collect_a_sequence_of_code_point(
            input, position,
            [](auto const c) {
                return !static_cast<bool(*)(code_point_t)>(
                    is_ascii_whitespace)(c);
            });

        append(tokens, token);

        skip_ascii_whitespace(input, position);
    }

    return tokens;
}

inline list<string_view_t>
split_on_commas(string_view_t const input)
{
    position_t position = 0;
    list<string_view_t> tokens;

    auto const past_the_end = length(input);
    while (position < past_the_end) {
        auto token = collect_a_sequence_of_code_point(
            input, position,
            [](auto const c) {
                return c != U'\x2C';
            });

        strip_leading_and_trailing_ascii_whitespace(token);

        append(tokens, token);

        if (position < past_the_end) {
            assert(input[position] == U'\x2C');
            ++position;
        }
    }

    return tokens;
}

template<typename String>
inline string_t
concatenate(list<String> const& list, string_view_t const separator = {})
{
    if (is_empty(list)) return U"";

    string_t result;

    for (auto const& item: list) {
        append(result, item);
        append(result, separator);
    }

    return result;
}

inline string_t
operator""_s(char32_t const* ptr, size_t const n)
{
    return { ptr, n };
}

inline string_view_t
operator""_sv(char32_t const* ptr, size_t const n)
{
    return { ptr, n };
}

//!@}
} // namespace whatwg::infra

#endif // WHATWG_INFRA_STRING_HPP
