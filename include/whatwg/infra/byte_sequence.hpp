//! \file
//! [4.4. Byte sequences](https://infra.spec.whatwg.org/#byte-sequences)
#ifndef WHATWG_INFRA_BYTE_SEQUENCE_HPP
#define WHATWG_INFRA_BYTE_SEQUENCE_HPP

#include "algorithm.hpp"
#include "byte_fwd.hpp"
#include "byte_sequence_fwd.hpp"
#include "string_fwd.hpp"

#include "detail/byte.hpp"

#include <algorithm>

#include <boost/algorithm/string/predicate.hpp>

namespace whatwg::infra {

//! \addtogroup byte_sequence Byte Sequence
//! [4.4. Byte sequences](https://infra.spec.whatwg.org/#byte-sequences)
//!@{

inline size_t
length(byte_sequence_view_t const bytes)
{
    return bytes.size();
}

inline bool
is_empty(byte_sequence_view_t const s)
{
    return s.empty();
}

inline void
append(byte_sequence_t& s, byte_t const b)
{
    s.push_back(b);
}

inline void
append(byte_sequence_t& s1, byte_sequence_view_t const s2)
{
    s1.append(s2);
}

inline void
prepend(byte_sequence_t& s, byte_t const b)
{
    s.insert(0, 1, b);
}

inline void
prepend(byte_sequence_t& s1, byte_sequence_view_t const s2)
{
    s1.insert(0, s2);
}

//! \brief Replace all bytes that match a given condition
//!        with the given byte, or do nothing if none match the condition.
template<typename Predicate>
void
replace(byte_sequence_t& s, Predicate condition, byte_t const c)
{
    std::replace_if(s.begin(), s.end(), condition, c);
}

//! \detail Do nothing if index is out of range.
inline void
insert(byte_sequence_t& s, position_t const index, byte_t const c)
{
    if (index > size(s)) return;

    s.insert(index, 1, c);
}

//! \param s1 target
//! \param s2 byte sequence to insert
//! \detail Do nothing if index is out of range.
inline void
insert(byte_sequence_t& s1, position_t const index, byte_sequence_view_t const s2)
{
    if (index > size(s1)) return;

    s1.insert(index, s2);
}

//! \brief Remove length bytes of subsequence from position pos.
//! \detail Do nothing if pos is out of range. If pos + len is out of range,
//!         then remove [pos, length(s)).
inline void
remove(byte_sequence_t& s, position_t const pos, size_t const len)
{
    if (pos >= length(s)) return;

    s.erase(pos, len);
}

//! \brief Remove all bytes that match a given condition,
//!        or do nothing if none matches.
template<typename Predicate>
void
remove(byte_sequence_t& s, Predicate condition)
{
    s.erase(
        std::remove_if(s.begin(), s.end(), condition),
        s.end()
    );
}

//! \brief Empty the byte sequence
inline void
empty(byte_sequence_t& s)
{
    s.clear();
}

inline bool
contains(byte_sequence_view_t const s, byte_t const c)
{
    return s.find(c, 0) != s.npos;
}

template<typename Predicate>
bool
contains(byte_sequence_view_t const s, Predicate condition)
{
    return std::any_of(s.begin(), s.end(), condition);
}

inline void
assign(byte_sequence_t& s1, byte_sequence_view_t const s2)
{
    s1.assign(s2);
}

inline byte_sequence_t
byte_lowercase_copy(byte_sequence_view_t const bytes);

inline void
byte_lowercase(byte_sequence_t& bytes)
{
    for (auto& b: bytes) {
        b = detail::to_lower(b);
    }
}

inline byte_sequence_t
byte_lowercase_copy(byte_sequence_view_t const bytes)
{
    byte_sequence_t result;

    result.reserve(bytes.size());

    for (auto const b: bytes) {
        result.push_back(detail::to_lower(b));
    }

    return result;
}

inline void
byte_uppercase(byte_sequence_t& bytes)
{
    for (auto& b: bytes) {
        b = detail::to_upper(b);
    }
}

inline byte_sequence_t
byte_uppercase_copy(byte_sequence_view_t const bytes)
{
    byte_sequence_t result;

    result.reserve(bytes.size());

    for (auto const b: bytes) {
        result.push_back(detail::to_upper(b));
    }

    return result;
}

inline bool
byte_case_insensitive_match(
    byte_sequence_t const& a, byte_sequence_t const& b)
{
    return boost::equals(a, b, detail::is_iequal);
}

inline string_t
isomorphic_decode(byte_sequence_view_t const input)
{
    string_t result;

    result.reserve(length(input));

    for (auto const c: input) {
        result.push_back(to_code_point(c));
    }

    return result;
}

inline bool
starts_with(byte_sequence_view_t const s1, byte_sequence_view_t const s2)
{
    namespace ba = boost::algorithm;
    return ba::starts_with(s1, s2);
}

inline bool
starts_with(byte_sequence_view_t const s, byte_t const b)
{
    return !is_empty(s) && s[0] == b;
}

//! \brief case-insensitive version of starts_with()
inline bool
istarts_with(byte_sequence_view_t const s1, byte_sequence_view_t const s2)
{
    namespace ba = boost::algorithm;
    return ba::istarts_with(s1, s2);
}

//! \brief case-insensitive version of starts_with()
inline bool
istarts_with(byte_sequence_view_t const s, byte_t const b)
{
    return !is_empty(s) && to_ascii_lower(s[0]) == to_ascii_lower(b);
}

inline bool
ends_with(byte_sequence_view_t const s1, byte_sequence_view_t const s2)
{
    namespace ba = boost::algorithm;
    return ba::ends_with(s1, s2);
}

inline bool
ends_with(byte_sequence_view_t const s, byte_t const b)
{
    return !is_empty(s) && s[length(s) - 1] == b;
}

//! \brief case-insensitive version of ends_with()
inline bool
iends_with(byte_sequence_view_t const s1, byte_sequence_view_t const s2)
{
    namespace ba = boost::algorithm;
    return ba::iends_with(s1, s2);
}

//! \brief case-insensitive version of ends_with()
inline bool
iends_with(byte_sequence_view_t const s, byte_t const b)
{
    return !is_empty(s) &&
        to_ascii_lower(s[length(s) - 1]) == to_ascii_lower(b);
}

inline byte_sequence_t
operator""_s(byte_t const* ptr, size_t const n)
{
    return { ptr, n };
}

inline byte_sequence_view_t
operator""_sv(byte_t const* ptr, size_t const n)
{
    return { ptr, n };
}

//!@}
} // namespace whatwg::infra

#endif // WHATWG_INFRA_BYTE_SEQUENCE_HPP
