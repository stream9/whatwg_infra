//! \file
//! [4.6. Strings](https://infra.spec.whatwg.org/#strings)
#ifndef WHATWG_INFRA_STRING_FWD_HPP
#define WHATWG_INFRA_STRING_FWD_HPP

#include "code_point_fwd.hpp"

#include <string>
#include <string_view>

namespace whatwg::infra {

//! \addtogroup string String
//! [4.6. Strings](https://infra.spec.whatwg.org/#strings)
//!@{

class string_view_t;

class string_t : public std::basic_string<code_point_t>
{
    using base_t = std::basic_string<code_point_t>;
public:
    using base_t::base_t;
    using base_t::operator=;

    // from base class
    string_t(base_t const& s) : base_t { s } {}
    string_t& operator=(base_t const& s)
    {
        return *this = string_t(s);
    }
};

class string_view_t : public std::basic_string_view<code_point_t>
{
    using base_t = std::basic_string_view<code_point_t>;
public:
    //! @name Constructor
    //! @{
    constexpr string_view_t() noexcept {}
    constexpr string_view_t(string_view_t const&) noexcept = default;

    constexpr string_view_t(value_type const* const s ,
                                   size_type const count) noexcept
        : base_t { s, count } {}

    constexpr string_view_t(value_type const* const s) noexcept
        : base_t { s } {}

    constexpr string_view_t(value_type const* const begin ,
                                   value_type const* const end) noexcept
        : base_t { begin, static_cast<size_type>(end - begin) } {}

    // with base_t
    constexpr string_view_t(base_t const& s) noexcept
        : base_t { s } {}

    string_view_t(string_t::const_iterator const begin,
                  string_t::const_iterator const end) noexcept
        : string_view_t { &*begin, &*end } {}

    // with std::u32string
    string_view_t(std::u32string const& s) noexcept
        : base_t { static_cast<base_t>(s) } {}
    //! @}

    //! @name Assignment
    //! @{
    constexpr string_view_t&
        operator=(string_view_t const&) noexcept = default;
    //! @}
};

using position_t = size_t;

//!@}
} // namespace whatwg::infra

#endif // WHATWG_INFRA_STRING_FWD_HPP
