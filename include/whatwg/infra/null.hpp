//! \file
//! [4.1. Nulls](https://infra.spec.whatwg.org/#nulls)
#ifndef WHATWG_INFRA_NULL_HPP
#define WHATWG_INFRA_NULL_HPP

#include "optional.hpp"

namespace whatwg::infra {

//! \defgroup null Null
//! [4.1. Nulls](https://infra.spec.whatwg.org/#nulls)
//!@{

template<typename T>
using nullable = optional<T>;

auto constexpr null = std::nullopt;

//!@}
} // namespace whatwg::infra

#endif // WHATWG_INFRA_NULL_HPP
