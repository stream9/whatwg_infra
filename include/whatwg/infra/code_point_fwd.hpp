//! \file
//! [4.5. Code points](https://infra.spec.whatwg.org/#code-points)
#ifndef WHATWG_INFRA_CODE_POINT_FWD_HPP
#define WHATWG_INFRA_CODE_POINT_FWD_HPP

namespace whatwg::infra {

//! \addtogroup code_point Code Point
//! [4.5. Code points](https://infra.spec.whatwg.org/#code-points)
//!@{

using code_point_t = char32_t;

//!@}
} // namespace whatwg::infra

#endif // WHATWG_INFRA_CODE_POINT_FWD_HPP
