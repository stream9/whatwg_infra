//! \file
//! [4.5. Code points](https://infra.spec.whatwg.org/#code-points)
#ifndef WHATWG_INFRA_CODE_POINT_HPP
#define WHATWG_INFRA_CODE_POINT_HPP

#include "algorithm.hpp"
#include "code_point_fwd.hpp"

namespace whatwg::infra {

//! \addtogroup code_point Code Point
//! [4.5. Code points](https://infra.spec.whatwg.org/#code-points)
//!@{

inline bool
is_valid_code_point(code_point_t const c)
{
    return c <= U'\x10FFFF';
}

inline bool
is_surrogate(code_point_t const c)
{
    return between(c, U'\xD800', U'\xDFFF');
}

inline bool
is_scalar_value(code_point_t const c)
{
    return is_valid_code_point(c) && !is_surrogate(c);
}

inline bool
is_noncharacter(code_point_t const c)
{
    return between(c, U'\xFDD0', U'\xFDEF')
        || c == U'\xFFFE'  || c == U'\xFFFF'  || c == U'\x1FFFE'
        || c == U'\x1FFFF' || c == U'\x2FFFE' || c == U'\x2FFFF'
        || c == U'\x3FFFE' || c == U'\x3FFFF' || c == U'\x4FFFE'
        || c == U'\x4FFFF' || c == U'\x5FFFE' || c == U'\x5FFFF'
        || c == U'\x6FFFE' || c == U'\x6FFFF' || c == U'\x7FFFE'
        || c == U'\x7FFFF' || c == U'\x8FFFE' || c == U'\x8FFFF'
        || c == U'\x9FFFE' || c == U'\x9FFFF' || c == U'\xAFFFE'
        || c == U'\xAFFFF' || c == U'\xBFFFE' || c == U'\xBFFFF'
        || c == U'\xCFFFE' || c == U'\xCFFFF' || c == U'\xDFFFE'
        || c == U'\xDFFFF' || c == U'\xEFFFE' || c == U'\xEFFFF'
        || c == U'\xFFFFE' || c == U'\xFFFFF' || c == U'\x10FFFE'
        || c == U'\x10FFFF';
}

inline bool
is_ascii_code_point(code_point_t const c)
{
    return c <= U'\x7F'; // 0 <= c is implied
}

inline bool
is_ascii_tab_or_newline(code_point_t const c)
{
    return c == U'\x09' || c == U'\x0A' || c == U'\x0D';
}

inline bool
is_ascii_whitespace(code_point_t const c)
{
    return c == U'\x09' || c == U'\x0A' || c == U'\x0C'
        || c == U'\x0D' || c == U'\x20';
}

inline bool
is_c0_control(code_point_t const c)
{
    return between(c, U'\x00', U'\x1F');
}

inline bool
is_c0_control_or_space(code_point_t const c)
{
    return is_c0_control(c) || c == U'\x20';
}

inline bool
is_control(code_point_t const c)
{
    return is_c0_control(c) || between(c, U'\x7F', U'\x9F');
}

inline bool
is_ascii_digit(code_point_t const c)
{
    return between(c, U'\x30', U'\x39');
}

inline bool
is_ascii_upper_hex_digit(code_point_t const c)
{
    return is_ascii_digit(c) || between(c, U'\x41', U'\x46');
}

inline bool
is_ascii_lower_hex_digit(code_point_t const c)
{
    return is_ascii_digit(c) || between(c, U'\x61', U'\x66');
}

inline bool
is_ascii_hex_digit(code_point_t const c)
{
    return is_ascii_upper_hex_digit(c) || is_ascii_lower_hex_digit(c);
}

inline bool
is_ascii_upper_alpha(code_point_t const c)
{
    return between(c, U'\x41', U'\x5A');
}

inline bool
is_ascii_lower_alpha(code_point_t const c)
{
    return between(c, U'\x61', U'\x7A');
}

inline bool
is_ascii_alpha(code_point_t const c)
{
    return is_ascii_upper_alpha(c) || is_ascii_lower_alpha(c);
}

inline bool
is_ascii_alphanumeric(code_point_t const c)
{
    return is_ascii_digit(c) || is_ascii_alpha(c);
}

inline code_point_t
to_ascii_upper(code_point_t const c)
{
    if (is_ascii_lower_alpha(c)) {
        return c - 0x20;
    }
    else {
        return c;
    }
}

inline code_point_t
to_ascii_lower(code_point_t const c)
{
    if (is_ascii_upper_alpha(c)) {
        return c + 0x20;
    }
    else {
        return c;
    }
}

//!@}
} // namespace whatwg::infra

#endif // WHATWG_INFRA_CODE_POINT_HPP
