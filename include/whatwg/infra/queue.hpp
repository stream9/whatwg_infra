//! \file
//! [5.1.2. Queues](https://infra.spec.whatwg.org/#queues)
#ifndef WHATWG_INFRA_QUEUE_HPP
#define WHATWG_INFRA_QUEUE_HPP

#include "stack.hpp"

#include <algorithm>
#include <deque>

namespace whatwg::infra {

//! \defgroup queue Queue
//! [5.1.2. Queues](https://infra.spec.whatwg.org/#queues)
//!@{

/*
 * type
 */
//! \brief A variation of list, designed as queue
//!
//! It is implemented as subclass of std::deque.
//!
//! **clone** operation isn't provided because of the same reason as [list](@ref list)
template<typename T>
class queue : public std::deque<T>
{
    using base_t = std::deque<T>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

/*
 * function
 */
//! \relates queue
//! \brief Insert item to the end of the queue.
template<typename T>
void
enqueue(queue<T>& q, T const& value)
{
    q.push_back(value);
}

//! \relates queue
//! \brief Move item to the end of the queue.
template<typename T>
void
enqueue(queue<T>& q, T&& value)
{
    q.push_back(std::move(value));
}

//! \relates queue
//! \brief Remove first item from queue and return it.
template<typename T>
optional<T>
dequeue(queue<T>& q)
{
    if (q.empty()) return {};

    T result = std::move(q.front());
    q.pop_front();

    return result;
}

//! \relates queue
//! \brief clear the queue
template<typename T>
void
empty(queue<T>& q)
{
    return q.clear();
}

//! \relates queue
//! \return True if queue s contains value, false otherwise.
template<typename T>
bool
contains(queue<T> const& q, T const& value)
{
    auto const it = std::find(q.begin(), q.end(), value);
    return it != q.end();
}

//! \relates queue
//! \return Size of the queue.
template<typename T>
size_t
size(queue<T> const& q)
{
    return q.size();
}

//! \relates queue
//! \return True if queue is empty, false otherwise.
template<typename T>
bool
is_empty(queue<T> const& q)
{
    return q.empty();
}

//!@}
} // namespace whatwg::infra

#endif // WHATWG_INFRA_QUEUE_HPP
