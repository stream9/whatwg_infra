#ifndef WHATWG_INFRA_DETAIL_STRING_HPP
#define WHATWG_INFRA_DETAIL_STRING_HPP

#include <algorithm>

namespace whatwg::infra::detail {

template<typename Iterator, typename Predicate>
size_t
count_while(Iterator it, Iterator const end, Predicate pred)
{
    size_t n = 0;

    for (; it != end; ++it, ++n) {
        if (!pred(*it)) break;
    }

    return n;
}

template<typename String, typename Predicate, typename Erase>
void
ltrim(String& s, Predicate pred, Erase erase)
{
    auto const n = count_while(s.begin(), s.end(), pred);

    erase(s, 0u, n);
}

template<typename String, typename Predicate, typename Erase>
void
rtrim(String& s, Predicate pred, Erase erase)
{
    auto const n = count_while(s.rbegin(), s.rend(), pred);

    if (n > 0) {
        auto const idx = s.size() - n;
        erase(s, idx, n);
    }
}

} // namespace whatwg::infra::detail

#endif // WHATWG_INFRA_DETAIL_STRING_HPP
