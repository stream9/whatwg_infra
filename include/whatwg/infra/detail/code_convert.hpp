#ifndef WHATWG_INFRA_DETAIL_CODE_CONVERT_HPP
#define WHATWG_INFRA_DETAIL_CODE_CONVERT_HPP

#include "../byte_sequence_fwd.hpp"
#include "../string_fwd.hpp"

#include <codecvt>
#include <locale>

namespace whatwg::infra::detail {

inline byte_sequence_t
to_utf8(string_view_t const s)
{
    using converter =
        std::wstring_convert<
            std::codecvt_utf8<char32_t>, char32_t >;

    static converter conv;

    return conv.to_bytes(s.begin(), s.end());
}

} // namespace whatwg::infra::detail


#endif // WHATWG_INFRA_DETAIL_CODE_CONVERT_HPP
