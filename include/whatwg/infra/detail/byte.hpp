#ifndef WHATWG_INFRA_DETAIL_BYTE_HPP
#define WHATWG_INFRA_DETAIL_BYTE_HPP

#include "../byte.hpp"

namespace whatwg::infra::detail {

inline bool
is_lower(byte_t const b)
{
    return '\x61' <= b && b <= '\x7A';
}

inline bool
is_upper(byte_t const b)
{
    return '\x41' <= b && b <= '\x5A';
}

inline byte_t
to_lower(byte_t const b)
{
    return is_upper(b) ? static_cast<byte_t>(b + 0x20) : b;
}

inline byte_t
to_upper(byte_t const b)
{
    return is_lower(b) ? static_cast<byte_t>(b - 0x20) : b;
}

inline bool
is_iequal(byte_t const a, byte_t const b)
{
    return to_lower(a) == to_lower(b);
}

} // namespace whatwg::infra::detail

#endif // WHATWG_INFRA_DETAIL_BYTE_HPP
