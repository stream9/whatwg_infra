#ifndef WHATWG_INFRA_BASE64_HPP
#define WHATWG_INFRA_BASE64_HPP

#include "byte_sequence.hpp"
#include "optional.hpp"
#include "string.hpp"

namespace whatwg::infra {

string_t
forgiving_base64_encode(byte_sequence_view_t);

optional<byte_sequence_t>
forgiving_base64_decode(string_view_t);

} // namespace whatwg::infra

#endif // WHATWG_INFRA_BASE64_HPP
