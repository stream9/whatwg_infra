#ifndef WHATWG_INFRA_ALGORITHM_HPP
#define WHATWG_INFRA_ALGORITHM_HPP

#include "byte_fwd.hpp"
#include "code_point_fwd.hpp"

#include <cassert>
#include <type_traits>

namespace whatwg::infra {

template<typename T,
    typename = std::enable_if_t<std::is_integral_v<T>> >
bool
between(T const v, T const from, T const to)
{
    return from <= v && v <= to;
}

inline byte_t
to_byte(code_point_t const c)
{
    assert(c <= U'\xFF');
    return static_cast<byte_t>(c);
}

inline code_point_t
to_code_point(byte_t const c)
{
    assert(0 <= c);
    return static_cast<code_point_t>(c);
}

} // namespace whatwg::infra

#endif // WHATWG_INFRA_ALGORITHM_HPP
