//! \file
//! [5.1. Lists](https://infra.spec.whatwg.org/#lists)
#ifndef WHATWG_INFRA_LIST_HPP
#define WHATWG_INFRA_LIST_HPP

#include <algorithm>
#include <cassert>
#include <vector>

namespace whatwg::infra {

//! \defgroup list List
//! [5.1. Lists](https://infra.spec.whatwg.org/#lists)
//!@{

/*
 * type
 */
//! \brief Specification type consisting of finite ordered sequence of items
//!
//! It is implemented as subclass of std::vector.
//!
//! Specification requires **clone** operation to produce shallow copy of the list,
//! but this implementation doesn't provide that operation since this
//! implementation adopted value semantics. If you do need shallow copy, you
//! have use other techniques such as list of references or list of shared_ptrs.
template<typename T>
class list : public std::vector<T>
{
    using base_t = std::vector<T>;
public:
    using difference_type = typename base_t::difference_type;

public:
    using base_t::base_t;
    using base_t::operator=;
};

using position_t = size_t;

/*
 * function
 */
//! \relates list
template<typename T>
void
append(list<T>& l, T const& item)
{
    l.push_back(item);
}

//! \relates list
template<typename T>
void
append(list<T>& l, T&& item)
{
    l.push_back(std::move(item));
}

//! \relates list
template<typename T>
void
prepend(list<T>& l, T const& item)
{
    l.insert(l.begin(), item);
}

//! \relates list
template<typename T>
void
prepend(list<T>& l, T&& item)
{
    l.insert(l.begin(), std::move(item));
}

//! \relates list
//! \brief Replace all items from the list that match a given condition
//!        with the given item, or do nothing if none do.
template<typename T, typename Predicate>
void
replace(list<T>& l, Predicate condition, T const& value)
{
    std::replace_if(l.begin(), l.end(), condition, value);
}

//! \relates list
template<typename T>
void
insert(list<T>& l, position_t const index, T const& item)
{
    using difference_type = typename list<T>::difference_type;
    assert(index <= size(l));

    auto const it = l.begin() + static_cast<difference_type>(index);
    l.insert(it, item);
}

//! \relates list
template<typename T>
void
insert(list<T>& l, position_t const index, T&& item)
{
    using difference_type = typename list<T>::difference_type;
    assert(index <= size(l));

    auto const it = l.begin() + static_cast<difference_type>(index);
    l.insert(it, std::move(item));
}

//! \relates list
//! \brief Remove all items from the list that match a given condition,
//!        or do nothing if none do.
template<typename T, typename Predicate>
void
remove(list<T>& l, Predicate condition)
{
    l.erase(
        std::remove_if(l.begin(), l.end(), condition),
        l.end()
    );
}

//! \relates list
//! \brief Empty the list
template<typename T>
void
empty(list<T>& l)
{
    l.clear();
}

//! \relates list
template<typename T>
bool
contains(list<T> const& l, T const& value)
{
    auto const it = std::find(l.begin(), l.end(), value);
    return it != l.end();
}

//! \relates list
template<typename T>
size_t
size(list<T> const& l)
{
    return l.size();
}

//! \relates list
template<typename T>
bool
is_empty(list<T> const& l)
{
    return l.empty();
}

//!@}
} // namespace whatwg::infra

#endif // WHATWG_INFRA_LIST_HPP
