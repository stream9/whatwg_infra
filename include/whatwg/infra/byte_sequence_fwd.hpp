//! \file
//! [4.4. Byte sequences](https://infra.spec.whatwg.org/#byte-sequences)
#ifndef WHATWG_INFRA_BYTE_SEQUENCE_FWD_HPP
#define WHATWG_INFRA_BYTE_SEQUENCE_FWD_HPP

#include "byte_fwd.hpp"

#include <string>
#include <string_view>

namespace whatwg::infra {

//! \addtogroup byte_sequence Byte Sequence
//! [4.4. Byte sequences](https://infra.spec.whatwg.org/#byte-sequences)
//!@{

class byte_sequence_view_t;

class byte_sequence_t : public std::basic_string<byte_t>
{
    using base_t = std::basic_string<byte_t>;
public:
    using base_t::base_t;
    using base_t::operator=;

    byte_sequence_t(byte_sequence_t const&) = default;
    byte_sequence_t(byte_sequence_t&&) = default;

    byte_sequence_t& operator=(byte_sequence_t const&) = default;
    byte_sequence_t& operator=(byte_sequence_t&&) = default;

    // from base class
    byte_sequence_t(base_t const& s) : base_t { s } {}
    byte_sequence_t(base_t&& s) : base_t { std::move(s) } {}

    byte_sequence_t& operator=(base_t const& s)
    {
        return *this = byte_sequence_t(s);
    }

    byte_sequence_t& operator=(base_t&& s)
    {
        return *this = byte_sequence_t(std::move(s));
    }
};

class byte_sequence_view_t : public std::basic_string_view<byte_t>
{
    using base_t = std::basic_string_view<byte_t>;
public:
    //! @name Constructor
    //! @{
    constexpr byte_sequence_view_t() noexcept {}
    constexpr byte_sequence_view_t(byte_sequence_view_t const&) noexcept = default;

    constexpr byte_sequence_view_t(value_type const* const s ,
                                   size_type const count) noexcept
        : base_t { s, count } {}

    constexpr byte_sequence_view_t(value_type const* const s) noexcept
        : base_t { s } {}

    constexpr byte_sequence_view_t(base_t const& s) noexcept
        : base_t { s } {}

    byte_sequence_view_t(std::string const& s) noexcept
        : base_t { static_cast<base_t>(s) } {}

    constexpr byte_sequence_view_t(value_type const* const begin ,
                                   value_type const* const end) noexcept
        : base_t { begin, static_cast<size_type>(end - begin) } {}

    byte_sequence_view_t(byte_sequence_t::const_iterator const begin,
                         byte_sequence_t::const_iterator const end) noexcept
        : byte_sequence_view_t { &*begin, &*end } {}

    //! @}

    //! @name Assignment
    //! @{
    constexpr byte_sequence_view_t&
        operator=(byte_sequence_view_t const&) noexcept = default;
    //! @}
};

using position_t = size_t;

//!@}
} // namespace whatwg::infra

#endif // WHATWG_INFRA_BYTE_SEQUENCE_FWD_HPP
