//! \file
//! [5.1.1. Stacks](https://infra.spec.whatwg.org/#stacks)
#ifndef WHATWG_INFRA_STACK_HPP
#define WHATWG_INFRA_STACK_HPP

#include "optional.hpp"

#include <algorithm>
#include <deque>

namespace whatwg::infra {

//! \defgroup stack Stack
//! [5.1.1. Stacks](https://infra.spec.whatwg.org/#stacks)
//!@{

/*
 * type
 */
//! \brief A variation of list, designed as stack
//!
//! It is implemented as subclass of std::deque.
//!
//! **clone** operation isn't provided because of the same reason as [list](@ref list)
template<typename T>
class stack : public std::deque<T>
{
    using base_t = std::deque<T>;
public:
    using base_t::base_t;
    using base_t::operator=;
};

/*
 * function
 */
template<typename T>
bool is_empty(stack<T> const&);

//! \relates stack
template<typename T>
void
push(stack<T>& s, T const& item)
{
    s.push_back(item);
}

//! \relates stack
template<typename T>
void
push(stack<T>& s, T&& item)
{
    s.push_back(std::move(item));
}

//! \relates stack
//! \brief Remove last item from stack and return it.
template<typename T>
optional<T>
pop(stack<T>& s)
{
    if (is_empty(s)) return {};

    T result = std::move(s.back());
    s.pop_back();

    return result;
}

//! \relates stack
//! \brief Empty the stack.
template<typename T>
void
empty(stack<T>& s)
{
    s.clear();
}

//! \relates stack
template<typename T>
bool
contains(stack<T> const& s, T const& value)
{
    auto const it = std::find(s.begin(), s.end(), value);
    return it != s.end();
}

//! \relates stack
template<typename T>
size_t
size(stack<T> const& s)
{
    return s.size();
}

//! \relates stack
template<typename T>
bool
is_empty(stack<T> const& s)
{
    return s.empty();
}

//!@}
} // namespace whatwg::infra

#endif // WHATWG_INFRA_STACK_HPP
