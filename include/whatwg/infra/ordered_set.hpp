//! \file
//! [5.1.3. Sets](https://infra.spec.whatwg.org/#sets)
#ifndef WHATWG_INFRA_ORDERED_SET_HPP
#define WHATWG_INFRA_ORDERED_SET_HPP

#include <algorithm>

#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/random_access_index.hpp>
#include <boost/multi_index_container.hpp>
#include <boost/range/irange.hpp>

namespace whatwg::infra {

//! \defgroup set Set
//! \brief #include <whatwg/infra/ordered_set.hpp>
//! \code
//! #include <whatwg/infra/ordered_set.hpp>
//! \endcode
//!@{

/*
 * type
 */
//! \brief A variation of list, designed as ordered_set.
//!        Entries are ordered by insertion order.
//!
//! It is implemented with boost::multi_index. std::set isn't suitable for
//! this because it doesn't retain insertion order.
//!
//! **clone** operation isn't provided because of the same reason as [list](@ref list)
template<typename T>
using ordered_set = boost::multi_index::multi_index_container<
    T,
    boost::multi_index::indexed_by<
        boost::multi_index::random_access<>,
        boost::multi_index::ordered_unique<
            boost::multi_index::identity<T> >
    > >;

/*
 * function
 */
template<typename T>
bool contains(ordered_set<T> const&, T const&);

//! \brief Insert an item to the end of the set.
template<typename T>
void
append(ordered_set<T>& s, T const& item)
{
    if (contains(s, item)) return;

    s.push_back(item);
}

//! \brief Move an item to the end of the set.
template<typename T>
void
append(ordered_set<T>& s, T&& item)
{
    if (contains(s, item)) return;

    s.push_back(std::move(item));
}

//! \brief Insert an item to the top of the set.
template<typename T>
void
prepend(ordered_set<T>& s, T const& item)
{
    if (contains(s, item)) return;

    s.push_front(item);
}

//! \brief Move an item to the top of the set.
template<typename T>
void
prepend(ordered_set<T>& s, T&& item)
{
    if (contains(s, item)) return;

    s.push_front(std::move(item));
}

//! \brief If the set contains item or replacement, then replace the first
//!        instance of either with replacement and remove all other instances.
//! \param item an item to replace from
//! \param replacement an item to replace to
template<typename T>
void
replace(ordered_set<T>& s, T const& item, T const& replacement)
{
    auto const end = s.end();
    auto match = [&](auto const& v) {
        return v == item || v == replacement;
    };

    auto const it = std::find_if(s.begin(), end, match);
    if (it == end) return;

    // erase first
    for (auto it2 = it + 1; it2 != end; ++it2) {
        if (match(*it2)) {
            s.erase(it2);
        }
    }

    // then, replace so it doesn't violate unique constraint
    s.replace(it, replacement);
}

//! \return True if s1 is a subset of s2, false otherwise
template<typename T>
bool
is_subset(ordered_set<T> const& s1, ordered_set<T> const& s2)
{
    auto& idx1 = s1.template get<1>();
    auto& idx2 = s2.template get<1>();

    return std::includes(
        idx2.begin(), idx2.end(),
        idx1.begin(), idx1.end());
}

//! \return True if s1 is a superset of s2, false otherwise
template<typename T>
bool
is_superset(ordered_set<T> const& s1, ordered_set<T> const& s2)
{
    auto& idx1 = s1.template get<1>();
    auto& idx2 = s2.template get<1>();

    return std::includes(
        idx1.begin(), idx1.end(),
        idx2.begin(), idx2.end());
}

//! \return An ordered_set that is an intersection of s1 and s2
template<typename T>
ordered_set<T>
intersection_of(ordered_set<T> const& s1, ordered_set<T> const& s2)
{
    ordered_set<T> result;
    auto& idx1 = s1.template get<1>();
    auto& idx2 = s2.template get<1>();

    std::set_intersection(
        idx1.begin(), idx1.end(),
        idx2.begin(), idx2.end(),
        std::back_inserter(result)
        );

    return result;
}

//! \return An set that is an union of s1 and s2
template<typename T>
ordered_set<T>
union_of(ordered_set<T> const& a, ordered_set<T> const& b)
{
    ordered_set<T> result;
    auto& idx1 = a.template get<1>();
    auto& idx2 = b.template get<1>();

    std::set_union(
        idx1.begin(), idx1.end(),
        idx2.begin(), idx2.end(),
        std::back_inserter(result)
        );

    return result;
}

//! \return An ordered_set that contains integers from n to m inclusive.
template<typename T>
ordered_set<T>
the_range(T const n, T const m)
{
    assert(n <= m);

    auto const r = boost::irange<T>(n, m + 1);

    return { r.begin(), r.end() };
}

//! \brief Empty the set.
template<typename T>
void
empty(ordered_set<T>& s)
{
    s.clear();
}

//! \return True if set s contains value, false otherwise.
template<typename T>
bool
contains(ordered_set<T> const& s, T const& value)
{
    auto const it = std::find(s.begin(), s.end(), value);
    return it != s.end();
}

//! \return Size of the set.
template<typename T>
size_t
size(ordered_set<T> const& s)
{
    return s.size();
}

//! \return True if set is empty, false otherwise.
template<typename T>
bool
is_empty(ordered_set<T> const& s)
{
    return s.empty();
}

//!@}
} // namespace whatwg::infra

#endif // WHATWG_INFRA_ORDERED_SET_HPP
