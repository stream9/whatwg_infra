//! \file
//! [5.2. Maps](https://infra.spec.whatwg.org/#maps)
#ifndef WHATWG_INFRA_ORDERED_MAP_HPP
#define WHATWG_INFRA_ORDERED_MAP_HPP

#include "ordered_set.hpp"

#include <initializer_list>

#include <boost/multi_index/member.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/random_access_index.hpp>
#include <boost/multi_index_container.hpp>

namespace whatwg::infra {

namespace bmi = boost::multi_index;

//! \defgroup map Map
//! [5.2. Maps](https://infra.spec.whatwg.org/#maps)
//!@{

//! \brief Finite ordered sequence of key/value pairs, with no key
//!        appearing twice. Entries are ordered by insertion order.
//!
//! It is implemented with boost::multi_index. std::map isn't suitable for
//! this because it doesn't retain insertion order.
template<typename Key, typename Value>
class ordered_map
{
public:
    struct entry_t {
        Key key;
        Value value;
    };

private:
    using container_t = bmi::multi_index_container<
        entry_t,
        bmi::indexed_by<
            bmi::random_access<>,
            bmi::ordered_unique<
                bmi::member<entry_t, Value, &entry_t::key> >
        > >;
public:
    using key_type = Key;
    using mapped_type = Value;
    using value_type = entry_t;
    using size_type = typename container_t::size_type;
    using const_iterator = typename container_t::const_iterator;

public:
    //!@{
    //! \name Constructor
    ordered_map() = default;
    ordered_map(ordered_map const&) = default;
    ordered_map(ordered_map&&) = default;

    ordered_map(std::initializer_list<entry_t> list)
        : m_map { list } {}
    //!@}

    //!@{
    //! \name Assignment
    ordered_map& operator=(ordered_map const&) = default;
    ordered_map& operator=(ordered_map&&) = default;
    //!@}

    //!@{
    //! \name Accessor

    //! \brief Access value of entry by key
    //! \tparam CompatibleKey any type which can be comparable to Key
    //! \return read-only reference of value
    //! \exception std::out_of_range if key doesn't exist in map
    template<typename CompatibleKey>
    Value const&
    operator[](CompatibleKey const& key) const
    {
        auto const it = find(key);
        if (it == end()) throw std::out_of_range(__PRETTY_FUNCTION__);

        return it->value;
    }

    //! \brief Access value of entry by key. Create new entry with given key
    //!        if map doesn't have entry with key.
    //! \tparam CompatibleKey any type which can be comparable to Key
    //! \return reference of value
    template<typename CompatibleKey>
    Value&
    operator[](CompatibleKey const& key)
    {
        auto const it = find(key);
        if (it != end()) {
            return const_cast<Value&>(it->value);
        }
        else {
            m_map.push_back(entry_t { key, {} });
            return const_cast<Value&>(m_map.back().value);
        }
    }
    //!@}

    //!@{
    //! \name Iterator

    //! \return insertion ordered random access iterator
    const_iterator
    begin() const
    {
        return m_map.begin();
    }

    //! \return insertion ordered random access iterator
    const_iterator
    end() const
    {
        return m_map.end();
    }
    //!@}

    //!@{
    //! \name Query

    //! \return number of entries in the map
    size_type
    size() const
    {
        return m_map.size();
    }

    //! \tparam CompatibleKey any type which can be comparable to Key
    //! \return iterator to the entry if entry is found, otherwise end iterator
    template<typename CompatibleKey>
    const_iterator
    find(CompatibleKey const& key) const
    {
        auto& idx = m_map.template get<1>();
        return m_map.template project<0>(idx.find(key));
    }

    //! \tparam CompatibleKey any type which can be comparable to Key
    //! \return true if map has entry which has given key, false otherwise
    template<typename CompatibleKey>
    bool
    contains(CompatibleKey const& key) const
    {
        return find(key) != end();
    }
    //!@}

    //!@{
    //! \name Modifier

    //! \return iterator that point right after the erased entry
    const_iterator
    erase(const_iterator const it)
    {
        return m_map.erase(it);
    }

    //!@}
private:
    container_t m_map;
};

/*
 * function
 */
//! \relates ordered_map
//! \tparam CompatibleKey any type which can be comparable to Key
//! \return read-only reference to the value if entry with key exists,
//!         otherwise nothing
template<typename Key, typename Value, typename CompatibleKey>
Value const*
get_the_value_of_an_entry(
        ordered_map<Key, Value> const& m, CompatibleKey const& k)
{
    auto const it = m.find(k);
    if (it == m.end()) return nullptr;

    return &it->value;
}

//! \relates ordered_map
//! \brief Update the value of an entry if map has an entry with given key,
//!        otherwise add a new entry with given key and value.
template<typename Key, typename Value>
void
set_the_value_of_an_entry(ordered_map<Key, Value>& m,
                          Key const& k, Value const& v)
{
    m[k] = v;
}

//! \relates ordered_map
//! \brief Remove all entries from the map that match a given condition
//!        or do nothing if none do.
//! \tparam Predicate unary predicate: bool(entry_t const&)
template<typename Key, typename Value, typename Predicate>
void
remove_an_entry(ordered_map<Key, Value>& map, Predicate condition)
{
    auto const end = map.end();
    for (auto it = map.begin(); it != end;) {
        if (condition(*it)) {
            it = map.erase(it);
        }
        else {
            ++it;
        }
    }
}

//! \relates ordered_map
//! \tparam CompatibleKey any type which can be comparable to Key
//! \return true if map has entry which has given key, false otherwise
template<typename Key, typename Value, typename CompatibleKey>
bool
contains_an_entry_with_a_given_key(ordered_map<Key, Value> const& map,
                                   CompatibleKey const& key)
{
    return map.contains(key);
}

//! \relates ordered_map
//! \return An new ordered_set whose items are each of keys in the map's entries
template<typename Key, typename Value>
ordered_set<Key>
get_the_keys(ordered_map<Key, Value> const& map)
{
    ordered_set<Key> result;
    result.reserve(map.size());

    for (auto const& entry: map) {
        append(result, entry.key);
    }

    return result;
}

//! \relates ordered_map
//! \return An new ordered_set whose items are each of values in the map's entries
template<typename Key, typename Value>
ordered_set<Value>
get_the_values(ordered_map<Key, Value> const& map)
{
    ordered_set<Value> result;
    result.reserve(map.size());

    for (auto const& entry: map) {
        append(result, entry.value);
    }

    return result;
}

//! \relates ordered_map
//! \return number of entires in the map
template<typename Key, typename Value>
size_t
size(ordered_map<Key, Value> const& map)
{
    return map.size();
}

//! \relates ordered_map
//! \return true if map is empty, false otherwise
template<typename Key, typename Value>
bool
is_empty(ordered_map<Key, Value> const& map)
{
    return map.size() == 0;
}

//!@}
} // namespace whatwg::infra

#endif // WHATWG_INFRA_ORDERED_MAP_HPP
