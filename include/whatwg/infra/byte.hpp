//! \file
//! [4.3. Byte](https://infra.spec.whatwg.org/#bytes)
#ifndef WHATWG_INFRA_BYTE_HPP
#define WHATWG_INFRA_BYTE_HPP

#include "byte_fwd.hpp"
#include "algorithm.hpp"

namespace whatwg::infra {

//! \addtogroup byte Byte
//! [4.3. Byte](https://infra.spec.whatwg.org/#bytes)
//!@{

inline bool
is_ascii_byte(byte_t const b)
{
    return 0 <= b; // b <= 0x7F is implied
}

inline bool
is_ascii_tab_or_newline(byte_t const c)
{
    return c == '\x09' || c == '\x0A' || c == '\x0D';
}

inline bool
is_ascii_whitespace(byte_t const c)
{
    return c == '\x09' || c == '\x0A' || c == '\x0C'
        || c == '\x0D' || c == '\x20';
}

inline bool
is_c0_control(byte_t const c)
{
    return between(c, '\x00', '\x1F');
}

inline bool
is_c0_control_or_space(byte_t const c)
{
    return is_c0_control(c) || c == '\x20';
}

inline bool
is_control(byte_t const c)
{
    return is_c0_control(c) || c == '\x7F';
}

inline bool
is_ascii_digit(byte_t const c)
{
    return between(c, '\x30', '\x39');
}

inline bool
is_ascii_upper_hex_digit(byte_t const c)
{
    return is_ascii_digit(c) || between(c, '\x41', '\x46');
}

inline bool
is_ascii_lower_hex_digit(byte_t const c)
{
    return is_ascii_digit(c) || between(c, '\x61', '\x66');
}

inline bool
is_ascii_hex_digit(byte_t const c)
{
    return is_ascii_upper_hex_digit(c) || is_ascii_lower_hex_digit(c);
}

inline bool
is_ascii_upper_alpha(byte_t const c)
{
    return between(c, '\x41', '\x5A');
}

inline bool
is_ascii_lower_alpha(byte_t const c)
{
    return between(c, '\x61', '\x7A');
}

inline bool
is_ascii_alpha(byte_t const c)
{
    return is_ascii_upper_alpha(c) || is_ascii_lower_alpha(c);
}

inline bool
is_ascii_alphanumeric(byte_t const c)
{
    return is_ascii_digit(c) || is_ascii_alpha(c);
}

inline byte_t
to_ascii_upper(byte_t const b)
{
    if (is_ascii_lower_alpha(b)) {
        return static_cast<byte_t>(b - 0x20);
    }
    else {
        return b;
    }
}

inline byte_t
to_ascii_lower(byte_t const b)
{
    if (is_ascii_upper_alpha(b)) {
        return static_cast<byte_t>(b + 0x20);
    }
    else {
        return b;
    }
}

//!@} Bytes
} // namespace whatwg::infra

#endif // WHATWG_INFRA_BYTE_HPP
