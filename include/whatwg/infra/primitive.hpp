#ifndef WHATWG_INFRA_PRIMITIVE_HPP
#define WHATWG_INFRA_PRIMITIVE_HPP

// 4. Primitive data types
#include "null.hpp"
#include "byte.hpp"
#include "byte_sequence.hpp"
#include "code_point.hpp"
#include "string.hpp"

#endif // WHATWG_INFRA_PRIMITIVE_HPP
