//! \file
//! [4.3. Byte](https://infra.spec.whatwg.org/#bytes)
#ifndef WHATWG_INFRA_BYTE_FWD_HPP
#define WHATWG_INFRA_BYTE_FWD_HPP

namespace whatwg::infra {

//! \addtogroup byte Byte
//! [4.3. Byte](https://infra.spec.whatwg.org/#bytes)
//!@{

using byte_t = char;

//!@} Bytes

} // namespace whatwg::infra

#endif // WHATWG_INFRA_BYTE_FWD_HPP
