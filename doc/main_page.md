# Overview

Headers
-------------
|Filename                       | Types, Functions
|-------------------------------|-------------------------------------------------
|whatwg/infra/primitive.hpp     | include 4 files below |
|whatwg/infra/byte.hpp          | byte_t and related algorithm
|whatwg/infra/byte_sequence.hpp | byte_sequence_t and related algorithms
|whatwg/infra/code_point.hpp    | code_point_t and related algorithms
|whatwg/infra/string.hpp        | string_t and related algorithms
| | |
|whatwg/infra/list.hpp          | list<T> and related algorithm
|whatwg/infra/stack.hpp         | stack<T> and related algorithm
|whatwg/infra/queue.hpp         | queue<T> and related algorithm
|whatwg/infra/ordered_set.hpp   | set<T> and related algorithm
|whatwg/infra/ordered_map.hpp   | map<Key, Value> and related algorithm
| | |
|whatwg/infra/base64.hpp        | Forgiving base64

